var test;
class JRCORE {
    constructor(server_url) {
        this.serverURL = server_url;
    }

    Init() {
        this.moveModals();
        this.addOnChangeFCE();
        test = this;
    }

    addOnChangeFCE() {
        const langSelector = document.getElementById('lang_selector');
        langSelector.addEventListener('change', (event) => {
            this.changeLang(event);
        });
    }

    moveModals() {
        var modals = document.getElementsByClassName("modal");
        var pos = document.getElementById("modal_section");
        for (let index = 0, len = modals.length; index < len; ++index) {
            pos.appendChild(modals[index]);
        }
    }
    procceddInvitation(id, action) {
        document.getElementById("inv_id").value = id;
        document.getElementById("action").value = action;
        document.getElementById("invitation").submit();
    }
    markNotificationsRead() {
        this.sendPOST("api/core/v1/set/notifications/read", "all=all");
    }
    deleteNotification(e, id) {
        this.sendPOST("api/core/v1/delete/notification", "notif_id=" + id);
        e.parentElement.parentElement.remove();
        document.getElementById("notifications_open").dataset.count--;
    }
    changeLang(event) {
        var selected = event.target.selectedOptions[0].value;
        var text = event.target.selectedOptions[0].text;
        this.createToast("Language changing to " + text + " (" + selected + ")",
                "Lang change request", "alert-primary", 90000, false, "filled");
        this.sendPOST("api/core/v1/set/language/change", "lang_code=" + selected,
                this.reloadPage);
    }
    reloadPage() {
        console.log("reload");
        location.reload();
    }
    createToast(content, title, allertType = 'alert', time = 10000, dissmissible = true, filled = "") {
        halfmoon.initStickyAlert({
            content: content,
            title: title,
            alertType: allertType,
            fillType: filled,
            hasDismissButton: dissmissible,
            timeShown: time
        });
    }
    requestError(event) {
        App.createToast("Status:" + event.status + ", " + event.body,
                "Request failled!", "alert-danger", 90000);
    }
    sendPOST(url, data, callDone = null, callError = this.requestError) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
//Send the proper header information along with the request
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () { // Call a function when the state changes.
            if (this.readyState != 4)
                return;
            console.log(this);
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                callDone(this);
            } else {
                callError(this);
            }
        };
        xhr.send(data);
    }
    fixTextAreaHeight() {
        var items = document.getElementsByClassName("text");
        for (var item of items) {
            item.style.height = item.scrollHeight + 10 + "px";
        }
    }
}