<?php

require '..//vendor/autoload.php';

use Tracy\Debugger;

session_start();
Debugger::enable(Debugger::DETECT, __DIR__ . '/app/temp/tracy');
Debugger::timer('app_start');
Debugger::$maxDepth = 10; // default: 3
Debugger::$maxLength = 150; // default: 150
$app = new \JR\CORE\app\App();
$app->setENVFile(__DIR__);
$app->start();
