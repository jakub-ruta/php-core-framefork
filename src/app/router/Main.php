<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\router;

/**
 * Description of Main
 *
 * @author jakub
 */
class Main extends Router {

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        parent::__construct($dep);
        \Tracy\Debugger::getBar()->addPanel(new \JR\CORE\bridges\tracy\RouterPanel($this, $this->dep->getRequest()->getPath()));
    }

//put your code here
    public function execute() {
        $this->register();
        try {
            $this->next = $this->select($this->request->getParsedPath());
            $this->next = new $this->next($this->dep);
        } catch (NotExistsException $exc) {
            $this->next = new WebRouter($this->dep);
        }
        try {
            $this->next->execute($this->dep);
        } catch (\JR\CORE\router\RedirectToError $exc) {
            $this->redirectToError($exc->getHTTPCode(), $exc->getMessage());
        } catch (RedirectError $exc) {
            $this->redirect($exc->getPath());
        } catch (LoggedInPolicy $exc) {
            $this->dep->getSession()->setMessages("You have to be logged in to view requested page", "danger");
            $this->redirect("login/?redirect=" . $this->dep->getRequest()->getPath());
        } catch (AdminLevelPolicy $exc) {
            $this->redirectToError(403, "You do not have required admin level (" . $exc->getMessage() . ") to do this action!");
        } catch (\JR\CORE\midleware\rights\RightViolationException $exc) {
            $this->redirectToError(403, "You do not have required right (" . $exc->getMessage() . ") to do this action!");
        }
    }

    private function register() {
        $this->addRoute("", \JR\CORE\router\WebRouter::class);
        $this->addRoute("gateway/ldap", \JR\CORE\routes\LDAPRouter::class);
        $this->addRoute("gateway/support", \JR\CORE\routes\SupportRouter::class);
        $this->addRoute("cronDeamon", \JR\CORE\controlers\cron\CronDeamon::class);
        $this->addRoute("api/core/v1", \JR\CORE\routes\api\core\v1\ApiRouter::class);
        $this->registerAppSecificsRouters();
        $keys = array_map('strlen', array_keys($this->routes));
        array_multisort($keys, SORT_ASC, $this->routes);
    }

    private function registerAppSecificsRouters() {
        if (file_exists("app/config/routers.json")) {
            $data = json_decode(file_get_contents("app/config/routers.json"), true);
            foreach ($data as $key => $value) {
                $this->addRoute($key, $value);
            }
        }
    }

}
