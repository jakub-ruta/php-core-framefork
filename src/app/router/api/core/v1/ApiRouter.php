<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\routes\api\core\v1;

/**
 * Description of ApiRouter
 *
 * @author jakub
 */
class ApiRouter extends \JR\CORE\router\Router {

    /**
     *
     * @var \JR\CORE\controler\APIControler
     */
    protected $next;

    //put your code here
    public function execute() {
        $this->register();
        try {
            $this->next = $this->select($this->request->getParsedPath("api/core/v1/"));
            $this->next = new $this->next($this->dep);
        } catch (NotExistsException $exc) {
            $this->redirectToError("404", "Path not exists");
            return;
        } catch (\JR\CORE\helpers\form\CSRFViolation $exc) {
            $this->redirectToError("403", "CSRF token is not valid");
            return;
        } catch (LoggedInPolicy $exc) {
            $this->dep->getSession()->setMessages("You have to be logged in to view requested page", "danger");
            $this->redirect("login/?redirect=" . $this->dep->getRequest()->getPath());
            return;
        } catch (AdminLevelPolicy $exc) {
            $this->redirectToError(403, "You do not have required admin level (" . $exc->getMessage() . ") to do this action!");
            return;
        } catch (\JR\CORE\midleware\rights\RightViolationException $exc) {
            $this->redirectToError(403, "You do not have required right (" . $exc->getMessage() . ") to do this action!");
            return;
        }
        $this->next->execute($this->dep);
        $latte = new \JR\CORE\views\JSONView($this->next->msg_success,
                $this->next->msg_error, $this->next->msg_warning,
                $this->next->data);
        $latte->send();
    }

    private function register() {
        $this->addRoute("error", \JR\CORE\controlers\api\core\v1\ErrorControler::class);
        $this->addRoute("set/user", \JR\CORE\controlers\api\core\v1\set\UserControler::class);
        $this->addRoute("set/notifications", \JR\CORE\controlers\api\core\v1\set\NotificationsControler::class);
        $this->addRoute("delete/notification", \JR\CORE\controlers\api\core\v1\delete\NotificationControler::class);
        $this->addRoute("set/webhooks", \JR\CORE\controlers\api\core\v1\set\WebHooksControler::class);
        $this->addRoute("set/language", \JR\CORE\controlers\api\core\v1\set\Language::class);
        $keys = array_map('strlen', array_keys($this->routes));
        array_multisort($keys, SORT_ASC, $this->routes);
    }

    public function redirect($path) {
        parent::redirect("api/core/v1/" . $path);
    }

    public function redirectToError($code, $msg) {
        $this->dep->getSession()->setErrorMessages($msg);
        header("Location: " . $this->request->getPathPrefix() . "api/core/v1/error/$code");
        header("Connection: close");
        return;
    }

}
