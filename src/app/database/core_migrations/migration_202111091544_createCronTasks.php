<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111091544_createCronTasks extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "cron_tasks");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "cron_tasks` (
 `q_id` int(11) NOT NULL AUTO_INCREMENT,
 `task_id` int(11) NOT NULL,
 `priority` int(11) NOT NULL DEFAULT 5,
 `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`data`)),
 `owner` int(11) NOT NULL DEFAULT 5,
 `due` datetime NOT NULL DEFAULT current_timestamp(),
 `expire` datetime DEFAULT NULL,
 `status` set('pending','running','completed','error','expired','aborted') COLLATE utf8_bin NOT NULL DEFAULT 'pending',
 `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`message`)),
 `elapsed_time` float DEFAULT NULL,
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 `updated` datetime DEFAULT NULL ON UPDATE current_timestamp(),
 `completed` datetime DEFAULT NULL,
 PRIMARY KEY (`q_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
        $Schema->rawTable(get_class($this), "cron_tasks", $raw);
    }

}
