<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202108141118_createUsersSession extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_sessions");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_sessions` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `token` VARCHAR(255) NOT NULL , `expire` INT NOT NULL DEFAULT '0' , `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL , `type` VARCHAR(16) NOT NULL DEFAULT 'normal' , `admin_id` INT NULL DEFAULT NULL ,  `IP` VARCHAR(32) NULL DEFAULT NULL, `browser` VARCHAR(64) NULL DEFAULT NULL ,  PRIMARY KEY (`id`), UNIQUE KEY `token` (`token`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "users_sessions", $raw);
    }

}
