<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202108311157_createTokens extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_tokens");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_tokens`  (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `token` varchar(255) COLLATE utf8_bin NOT NULL,
 `user_id` int(11) DEFAULT NULL,
 `expire` int(11) NOT NULL,
 `created` datetime NOT NULL DEFAULT current_timestamp(),
 `type` varchar(64) COLLATE utf8_bin NOT NULL,
 `data` text COLLATE utf8_bin DEFAULT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
        $Schema->rawTable(get_class($this), "users_tokens", $raw);
    }

}
