<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202108311655_createUserDevices extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_devices");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_devices`  ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `device_id` VARCHAR(128) NOT NULL , `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL , `device_os` VARCHAR(32) NULL DEFAULT NULL , `device_model` VARCHAR(128) NULL DEFAULT NULL , PRIMARY KEY (`id`), INDEX (`user_id`), UNIQUE (`device_id`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "users_devices", $raw);
    }

}
