<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108121346_createUserCreditals
 *
 * @author jakub
 */
class migration_202108121346_createUserCreditals extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_creditals");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_creditals` ( `user_internal_id` INT NOT NULL , `password` VARCHAR(128) NOT NULL , `salt1` VARCHAR(16) NULL DEFAULT NULL , `salt2` VARCHAR(32) NULL DEFAULT NULL , `type` VARCHAR(8) NOT NULL DEFAULT 'jr_hash' , `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP , `updated` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL , PRIMARY KEY (`user_internal_id`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "users_creditals", $raw);
    }

}
