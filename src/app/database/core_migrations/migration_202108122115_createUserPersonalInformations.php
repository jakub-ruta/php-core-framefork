<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108122115_createUserPersonalInformations
 *
 * @author jakub
 */
class migration_202108122115_createUserPersonalInformations extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "users_personal_informations");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "users_personal_informations` ( `user_internal_id` INT NOT NULL , `first_name` VARCHAR(32) NOT NULL , `last_name` VARCHAR(32) NOT NULL , `birth_date` DATE NULL DEFAULT NULL , `country` VARCHAR(32) NULL DEFAULT '' , `city` VARCHAR(32) NULL DEFAULT '' , `zip` VARCHAR(16) NULL DEFAULT '' , `adress` TEXT NULL DEFAULT '' , `company` VARCHAR(32) NULL DEFAULT '' , `phone` VARCHAR(32) NULL DEFAULT '' , `mobile` VARCHAR(32) NULL DEFAULT '' , `gender` VARCHAR(16) NULL DEFAULT '' , `updated` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL , `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`user_internal_id`), UNIQUE (`mobile`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "users_personal_informations", $raw);
    }

}
