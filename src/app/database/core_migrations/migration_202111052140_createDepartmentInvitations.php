<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111052140_createDepartmentInvitations extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "departments_invitations");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "departments_invitations` ( `id` INT NOT NULL AUTO_INCREMENT , `admin_id` INT NOT NULL , `user_id` INT NOT NULL , `department_id` INT NOT NULL , `state` SET('pending','decelined','accepted','withdrawn') NOT NULL DEFAULT 'pending' , `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT NULL , `role` TINYINT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "departments_invitations", $raw);
    }

}
