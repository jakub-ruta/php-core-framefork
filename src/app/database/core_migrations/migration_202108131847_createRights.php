<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108131847_createRights
 *
 * @author jakub
 */
class migration_202108131847_createRights extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "rights");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "rights` ( `right_id` INT NOT NULL AUTO_INCREMENT , `right_name` VARCHAR(32) NOT NULL , `right_action` VARCHAR(16) NOT NULL DEFAULT 'view' , `right_description` TEXT NULL DEFAULT NULL , `right_granteable_by` TINYINT NOT NULL DEFAULT '9' , `visible_from` TINYINT NOT NULL DEFAULT '4' , PRIMARY KEY (`right_id`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "rights", $raw);
    }

}
