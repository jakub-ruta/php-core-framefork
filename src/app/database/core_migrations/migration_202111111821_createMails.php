<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111111821_createMails extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "mails");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "mails` ( `id` INT NOT NULL AUTO_INCREMENT , `subject` VARCHAR(255) NOT NULL , `content` TEXT NOT NULL , `status` SET('new','sended','error','pending') NOT NULL DEFAULT 'new' , `send_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "mails", $raw);
    }

}
