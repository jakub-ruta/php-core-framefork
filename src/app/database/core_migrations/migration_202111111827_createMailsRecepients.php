<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of migration_202108141118_createUsersSession
 *
 * @author jakub
 */
class migration_202111111827_createMailsRecepients extends \JR\CORE\database\migrations\Migrations {

    public function down(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->deleteTable(get_class($this), "mails_recepients");
    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $raw = "CREATE TABLE `" . $_ENV['DB_prefix'] . "mails_recepients` ( `id` INT NOT NULL AUTO_INCREMENT , `mail_id` INT NOT NULL , `mail` VARCHAR(128) NULL DEFAULT NULL , `status` SET('new','pending','sended','error') NOT NULL DEFAULT 'new' , `sent` DATETIME NULL DEFAULT NULL , `message` TEXT NULL DEFAULT NULL , `user_id` INT NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $Schema->rawTable(get_class($this), "mails_recepients", $raw);
    }

}
