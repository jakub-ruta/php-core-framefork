<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\database\migrations;

/**
 * Description of Table
 *
 * @author jakub
 */
class Table {

    protected $mode = null;
    protected $name = '';
    protected $raw = null;

    public function __construct($name, $mode, $raw = null) {
        $this->mode = $mode;
        $this->name = $name;
        $this->raw = $raw;
    }

    public function execute(\MysqliDb $db, $for_key_check = true) {
        switch ($this->mode) {
            case 'delete':
                $sql = "DROP TABLE IF EXISTS $this->name ;";
                break;
            case 'raw':
                $sql = $this->raw;
                break;
            default:
                throw new Exception("method not implemented");
        }
        if (!$for_key_check)
            $db->rawQuery('SET FOREIGN_KEY_CHECKS = 0;');
        $db->rawQuery($sql);

        if (!$for_key_check)
            $db->rawQuery('SET FOREIGN_KEY_CHECKS = 1;');
        if ($db->getLastError()) {
            throw new MigrationStuckException($db->getLastError());
        }
    }

}
