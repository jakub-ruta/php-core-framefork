<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108101241_createDefaultUsers
 *
 * @author jakub
 */
class seeder_202108101241_createDefaultUsers extends \JR\CORE\database\migrations\Migrations {

    //put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $dataKeys = array("internal_id", "login_name", "nickname",
            "main_role", "origin");
        $multiInsertData = array(
            array("1", "Guest", "Guest", "-1", "system"),
            array("5", "System", "System", "-10", "system"),
            array("6", "CLI", "CommandLineInterface", "-10", "system"),
            array("9", "CronDeamon", "CronDeamon", "-10", "system"),
        );
        $Schema->getDB()->insertMulti("users", $multiInsertData, $dataKeys);
        $Schema->finishMigration(get_class($this));
    }

}
