<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\migrations\core_migrations;

/**
 * Description of seeder_202108132212_createRights
 *
 * @author jakub
 */
class seeder_202109081206_AddRightToAllRightsGroup extends \JR\CORE\database\migrations\Migrations {

//put your code here
    public function down(\JR\CORE\database\migrations\Schema $Schema) {

    }

    public function up(\JR\CORE\database\migrations\Schema $Schema) {
        $Schema->startMigration(get_class($this));
        $db = $Schema->getDB();
        $id = $db->where('name', 'default_all')->getValue('rights_groups', 'id');
        $rights = $db->get('rights');
        $dataKeys = array("right_id",
            "group_id");
        $multiInsertData = array();
        foreach ($rights as $value) {
            $multiInsertData[] = array($value['right_id'], $id);
        }
        $db->insertMulti("rights_groups_assign", $multiInsertData, $dataKeys);
        //throw new \Exception($db->getLastQuery());
        $Schema->finishMigration(get_class($this));
    }

}
