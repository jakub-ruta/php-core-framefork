<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\departments;

/**
 * Description of DeparmentsInvitations
 *
 * @author jakub
 */
class DeparmentsInvitations extends \JR\CORE\midleware\Midleware {

    /**
     *
     * @var string name of table in db (without prefix)
     */
    protected $tableName = "departments_invitations";

    public function inviteUser($id, $admin_id, $user_id, $role) {
        if ($this->hasOpenInvitation($id, $user_id)) {
            throw new UserAlreadyHaveInvitation();
        }
        if ($role < 0) {
            $role = 0;
        }
        if ($role > 5) {
            $role = 5;
        }
        return parent::insert(['admin_id' => $admin_id,
                    'user_id' => $user_id,
                    'department_id' => $id,
                    'role' => $role]);
    }

    public function getByDepartment($id) {
        return $this->db->where("department_id", $id)
                        ->where("(created>? OR state=?) ", [date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 7),
                            'pending'])
                        ->join("users u", 'i.user_id=u.internal_id')
                        ->join('users u2', 'i.admin_id=u2.internal_id')
                        ->orderBy("i.created")
                        ->get($this->tableName . " i", null,
                                'i.*, u.*, u2.internal_id AS admin_internal_id,'
                                . 'u2.login_name AS admin_login_name,'
                                . 'u2.nickname AS admin_nickname,'
                                . 'u2.avatar AS admin_avatar');
    }

    public function hasOpenInvitation($id, $user_id) {
        return $this->db->where("department_id", $id)
                        ->where("user_id", $user_id)
                        ->where("state", "pending")
                        ->getValue($this->tableName, "id");
    }

    public function setStatus($inv_id, $state) {
        if (!in_array($state, ['pending', 'decelined', 'accepted', 'withdrawn'])) {
            throw new Exception("Not valid state");
        }
        return parent::update($inv_id, ['state' => $state]);
    }

    public function getAllForUser($user_id) {
        return $this->db->where("i.user_id", $user_id)
                        ->where("i.state", "pending")
                        ->join("users u", "i.admin_id=u.internal_id")
                        ->join("departments d", "i.department_id=d.id")
                        ->get($this->tableName . " i", null, "i.*, d.name, d.code, "
                                . "u.login_name, u.nickname, u.avatar");
    }

}
