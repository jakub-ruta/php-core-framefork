<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\cron;

/**
 * Description of Cron
 *
 * @author jakub
 */
class Cron extends \JR\CORE\midleware\Midleware {

    /**
     *
     * @var string name of table in db (without prefix)
     */
    protected $tableName = "cron_tasks";

    /**
     *
     * @var string primary key for col on db
     */
    protected $primaryKey = "q_id";

    public function setRunning($task_id) {
        $this->db->startTransaction();
        $this->db->setLockMethod("WRITE");
        $this->db->lock($this->tableName);
        if ($this->db->where("q_id", $task_id)
                        ->getValue($this->tableName, "status") == "pending") {
            $this->db->where("q_id", $task_id)
                    ->update($this->tableName, array("status" => "running"));
        } else {
            $this->db->unlock();
            $this->db->rollback();
            throw new CronNotNewException();
        }
        $this->db->commit();
        $this->db->unlock();
    }

    public function getNextTasks($max_priority = 10, $limit = 10) {
        return $this->db->where("q.priority", $max_priority, "<")
                        ->where("q.due", date("Y-m-d H:i:s"), "<")
                        ->where("q.status", "pending")
                        ->join("cron_tasks_overview o", "q.task_id=o.id")
                        ->orderBy("due", "ASC")
                        ->orderBy("priority", "ASC")
                        ->get($this->tableName . " q", $limit);
        return $this->db->where("priority", $max_priority, "<")
                        ->join("cron_tasks_overview r", "l.q_id=r.id")
                        ->orderBy("due", "ASC")
                        ->where("due", date("Y-m-d H:i:s"), "<")
                        ->where("status", "pending")
                        ->orderBy("priority", "ASC")
                        ->get("cron_tasks l", $limit);
    }

    public function setDone($task_id, $data, $time) {
        $this->db->where("q_id", $task_id)
                ->update($this->tableName, array("status" => "completed",
                    "message" => json_encode($data)
                    , "completed" => date("Y-m-d H:i:s"),
                    "elapsed_time" => microtime(true) - $time));
    }

    public function setError($task_id, $data, $time) {
        $this->db->where("q_id", $task_id)
                ->update($this->tableName, array("status" => "error",
                    "message" => json_encode($data)
                    , "completed" => date("Y-m-d H:i:s"),
                    "elapsed_time" => microtime(true) - $time));
    }

    public function setExpired($task_id) {
        $this->db->where("q_id", $task_id)
                ->update($this->tableName, array("status" => "expired")
        );
    }

    /**
     * Get list of all available tasks
     * @return array
     */
    public function getList() {
        return $this->db->get("cron_tasks_overview");
    }

    public function getLastQueued($task_id) {
        return $this->db->where("task_id", $task_id)
                        ->orderBy("due", "DESC")
                        ->getOne($this->tableName);
    }

    public function addMulti($multiData) {
        return $this->db->insertMulti($this->tableName, $multiData);
    }

    public function deleteOld($time) {
        $this->db->where("due", date("Y-m-d H:i:s", time() - $time), "<")
                ->delete($this->tableName);
        return $this->db->count;
    }

    public function getUpcommingTasks($limit = 10, $offset = 0, $id = null) {
        if ($id) {
            $this->db->where("task_id", $id);
        }
        return $this->db->where("l.status", "pending")
                        ->where("l.due", date("Y-m-d H:i:s", time() - 1), ">")
                        ->orderBy("l.due", "ASC")
                        ->join("cron_tasks_overview r", "l.task_id=r.id")
                        ->join("users u", "l.owner=u.internal_id")
                        ->get($this->tableName . " l", [$offset, $limit],
                                "r.name, l.priority, l.due, l.owner, l.q_id, r.id,"
                                . " u.avatar, u.login_name");
    }

    public function getPastTasks($limit = 100, $offset = 0, $id = null) {
        if ($id) {
            $this->db->where("task_id", $id);
        }
        return $this->db->where("l.due", date("Y-m-d H:i:s"), "<")
                        ->orderBy("l.due")
                        ->join("cron_tasks_overview r", "l.task_id=r.id")
                        ->join("users u", "l.owner=u.internal_id")
                        ->get($this->tableName . " l", [$offset, $limit],
                                "r.name, l.priority, l.due, l.owner, l.q_id, r.id,"
                                . " u.avatar, u.login_name, l.status, l.message,"
                                . " l.elapsed_time, l.completed");
    }

    public function getOneTask($id) {
        return $this->db->where("id", $id)->getOne("cron_tasks_overview");
    }

    public function addToQueue($task_id, $owner, $due = null, $priority = 5, $data = null, $expire = null) {
        $task = $this->getOneTask($task_id);
        $INSdata = ["task_id" => $task_id,
            "priority" => $priority,
            "data" => json_encode(isset($data) ? $data : $task['default_data']),
            "owner" => $owner,
            "due" => isset($due) ? $due : date("Y-m-d H:i:s"),
            "expire" => date("Y-m-d H:i:s", $task['default_expiry'] + time())];
        return $this->insert($INSdata);
    }

    public function abortUpccomingTaskDisptach() {
        return $this->db->where("task_id", 1)
                        ->where("status", "pending")
                        ->update("cron_tasks", ['status' => 'aborted',
                            'message' => json_encode('duplicate tasks removed')]);
    }

}
