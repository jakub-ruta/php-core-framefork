<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\mail;

/**
 * Description of Mail
 *
 * @author jakub
 */
class Mail extends \JR\CORE\midleware\Midleware {

    /**
     *
     * @var string name of table in db (without prefix)
     */
    protected $tableName = "mails";

    public function getOneForSend($id) {
        $this->db->lock($this->tableName);
        $row = parent::getOne($id);
        if ($row['status'] != "new") {
            throw new MailNotNew();
        }
        parent::update($id, ['status' => "pending"]);
        $this->db->unlock();
        return $row;
    }

    public function getRecepients($id) {
        return $this->db->where("mail_id", $id)
                        ->get("mails_recepients");
    }

    /**
     *
     * @param int $id
     * @param array $recepients
     */
    public function addRecepients($id, $recepients) {
        $multi = [];
        foreach ($recepients as $r) {
            $multi[] = ['mail_id' => $id, "mail" => $r];
        }
        if (count($multi) > 0) {
            $this->db->insertMulti("mails_recepients", $multi);
        }
        return true;
    }

    public function setSendStatus($id, $status, $msg, $date = null) {
        return $this->db->where("id", $id)
                        ->update("mails_recepients",
                                ['status' => $status,
                                    'sent' => $date,
                                    'message' => $msg]);
    }

    public function setStatus($id, $status) {
        parent::update($id, ['status' => $status]);
    }

    public function getMailsForSend() {
        return $this->db->where("status", "new")
                        ->where("send_at", date("Y-m-d H:i:s"), "<")
                        ->get($this->tableName);
    }

}
