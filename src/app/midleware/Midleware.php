<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware;

/**
 * Description of Midleware
 *
 * @author jakub
 */
class Midleware {

    /**
     *
     * @var string primary key for col on db
     */
    protected $primaryKey = "id";

    /**
     *
     * @var array|null|array of arrays visible cols for each admin level, if array is null all is displaied.
     */
    protected $vissible = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10");

    /**
     *
     * @var array|null|array of arrays visible fields for each admin level, if array is null all is displaied.
     */
    protected $editable = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10");

    /**
     *
     * @var string name of table in db (without prefix)
     */
    protected $tableName;

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    /**
     * Constructor of MidleWare
     * @param \MysqliDb $db
     */
    public function __construct(\MysqliDb $db) {
        $this->db = $db;
    }

    /**
     * Function for get all rows from db limited by parasm
     * @param int $limit for limit returned rows
     * @param int $offset for get rows from this row
     * @return array of data and count returned from db
     */
    public function getAll($limit = 1000, $offset = 0) {
        $data = $this->db->get($this->tableName, array($offset, $limit));
        $count = $this->db->getValue($this->tableName, "COUNT(*)");
        return array($data, $count);
    }

    /**
     * Function for return always one row from db based on condition
     * @param string|int $value filtered value
     * @param string $key name of col in db
     * @return type
     */
    public function getOne($value, $key = null) {
        $this->db->where($key == null ? $this->primaryKey : $key, $value);
        return $this->db->getOne($this->tableName);
    }

    /**
     * Function for update one row id db
     * @param string $value value for filter If not set primary key will be used
     * @param type $data array of new data with format key => value
     * @param type $key key for filter
     * @return bool is success
     */
    public function update($value, $data, $key = null) {
        $this->db->where($key == null ? $this->primaryKey : $key, $value);
        return $this->db->update($this->tableName, $data);
    }

    /**
     * Function for add row to db
     * @param array $data data with format key => value
     * @return int id of last inserted row
     * @throws DuplicateRowException
     * @throws \Exception for general error
     */
    public function insert($data) {
        $this->db->insert($this->tableName, $data);
        $err = $this->db->getLastError();
        if ($err) {
            if (str_starts_with($err, "Duplicate entry")) {
                throw new DuplicateRowException($err);
            }
            throw new \Exception($this->db->getLastError());
        }
        return $this->db->getInsertId();
    }

    /**
     *
     * @param array $data in format key => value
     * @return int id of last inserted row
     * @throws \Exception general error
     */
    public function insertOrUpdate($data) {
        $update = array_keys($data);
        $this->db->onDuplicate($update, $this->primaryKey);
        $this->db->insert($this->tableName, $data);
        $err = $this->db->getLastError();
        if ($err) {
            throw new \Exception($this->db->getLastError());
        }
        return $this->db->getInsertId();
    }

}
