<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\tokens;

/**
 * Description of Session
 *
 * @author jakub
 */
class Tokens extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "id";
    protected $vissible = null;
    protected $editable = null;
    protected $tableName = "users_tokens";
    protected static $tableNameStatic = "users_tokens";
    protected $userId;

    public function __construct(\MysqliDb $db, $userId) {
        parent::__construct($db);
        $this->userId = $userId;
    }

    public function getAll($userId = null, $active = false): array {
        if ($userId == null)
            $userId = $this->userId;
        if ($active)
            $this->db->where('expire', time(), '>');
        else {
            $this->db->where('expire', time() - 60 * 60 * 24 * 2, '>');
        }
        return $this->db->orderBy('expire')
                        ->where("user_id", $userId)->get($this->tableName);
    }

    public function insert($type, $expire, $add_data = null) {
        $token = \JR\CORE\helpers\StringUtils::generate_string(235);
        $expire = time() + $expire;
        $data = ['user_id' => $this->userId,
            'token' => $token,
            'expire' => $expire,
            'type' => $type,
            'dats' => $add_data];
        parent::insert($data);
        return [$token, $expire];
    }

    public function InvalideToken($id) {
        return parent::update($id, ['expire' => 1]);
    }

    public function invalidateAll($user_id = null) {
        return parent::update($user_id ? $user_id : $this->userId, ['expire' => 1], "user_id");
    }

    public function insertOrUpdate($data) {
        throw new \Exception("Not possible in this class");
    }

    public static function getUserToken($token, $user, \MysqliDb $db) {
        return $db->where("user_id", $user)
                        ->where("token", $token)
                        ->getOne(self::$tableNameStatic);
    }

    public function destroyToken($token) {
        $data = ['expire' => 10];
        return parent::update($token, $data, "token");
    }

    public function createOne($user_id, $expire, $type) {
        $token = \JR\CORE\helpers\StringUtils::generate_string(150);
        parent::insert(['user_id' => $user_id,
            'expire' => time() + $expire,
            'type' => $type,
            'token' => $token]);
        return $token;
    }

    public function checkToken($token, $type) {
        $t = $this->db->where("token", $token)
                ->getOne($this->tableName);
        if ($t['type'] != $type) {
            throw new WrongTokenType();
        }
        if ($t['user_id'] != $this->userId) {
            throw new TokenNotBelongsToUser();
        }
        if ($t['expire'] < time()) {
            throw new TokenExpired();
        }
        return [$t['id'], $t['data']];
    }

}
