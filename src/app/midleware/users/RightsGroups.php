<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\rights;

/**
 * Description of Rights
 *
 * @author jakub
 */
class RightsGroups {

    protected $userInternalId;

    /**
     *
     * @var \MysqliDb
     */
    protected $db;
    protected $editable = ['note', 'role'];

    /**
     * Name of table in DB
     * @var string
     */
    protected $tableName = "rights_groups_assign";

    public function __construct($userInternalId, \MysqliDb $db) {
        $this->userInternalId = $userInternalId;
        $this->db = $db;
    }

    /**
     * return all groups for role and down
     * @param int from 0 to 10 $role
     * @return array
     */
    public function getGroups($role) {
        return $this->db->where('role', $role, '<=')
                        ->get('rights_groups');
    }

    public function getGroupsByDepartements($departments) {
        $departments[] = -1;
        return $this->db->where('l.department_id', $departments, 'IN')
                        ->orderBy('d.name', 'ASC')
                        ->orderBy('l.name', 'ASC')
                        ->orderBy('l.role', 'ASC')
                        ->join('departments d', 'l.department_id=d.id', 'LEFT')
                        ->get('rights_groups l', null, 'l.*, d.name AS dep_name, d.code AS dep_code');
    }

    public function getRightsForGroup($id) {
        $rights = array();
        $data = $this->db->rawQuery("SELECT l.*, r.group_id FROM rights l "
                . "LEFT JOIN rights_groups_assign r "
                . "ON (l.right_id=r.right_id "
                . "AND (r.group_id=? OR r.group_id IS NULL))", array($id));
        foreach ($data as $value) {
            $rights[$value['right_name']][$value['right_action']] = $value;
        }
        return $rights;
    }

    public function getOne($id) {
        return $this->db->where('r.id', $id)
                        ->join('departments d', 'r.department_id=d.id', 'LEFT')
                        ->getOne('rights_groups r', 'r.*, d.name AS dep_name');
    }

    public function getRightsINGroupsByIdsUnderRole($ids, $max_role = 11) {
        if (count($ids) > 0) {
            return $this->db->join("rights r", "l.right_id=r.right_id")
                            ->groupBy("r.right_id")
                            ->where("l.group_id", $ids, "IN")
                            ->where("r.right_granteable_by", $max_role, "<=")
                            ->get($this->tableName . " l");
        } else {
            return null;
        }
    }

    public function update($data, $id) {
        $this->db->where("id", $id);
        return $this->db->update("rights_groups",
                        array_intersect_key($data, array_flip($this->editable)));
    }

    public function setRights($id, $data) {
        $this->db->where("group_id", $id)
                ->delete($this->tableName);
        $new = array();
        foreach ($data as $key => $u) {
            $new[] = [$id, str_replace("right::", "", $key)];
        }
        $this->db->insertMulti($this->tableName, $new,
                ['group_id', 'right_id']);
    }

    public function create($data) {
        if (!isset($data['department_id'])) {
            $data['department_id'] = -1;
        }
        $this->db->insert("rights_groups",
                array_intersect_key($data, array_flip(['name', 'department_id'])));
        if ($this->db->getLastError()) {
            throw new \Exception($this->db->getLastError());
        }
        return $this->db->getInsertId();
    }

    public function delete($id) {
        $this->db->where("group_id", $id)
                ->delete($this->tableName);
        $this->db->where("id", $id)
                ->delete("rights_groups");
    }

}
