<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\sessions;

/**
 * Description of Session
 *
 * @author jakub
 */
class Session extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "id";
    protected $vissible = null;
    protected $editable = null;
    protected $tableName = "users_sessions";
    protected static $tableNameStatic = "users_sessions";
    protected $userId;
    protected $longExpire = 60 * 60 * 24;
    protected $shortExpire = 600;

    public function __construct(\MysqliDb $db, $userId) {
        parent::__construct($db);
        $this->userId = $userId;
    }

    public function getAll($userId = null, $active = false): array {
        if ($userId == null)
            $userId = $this->userId;
        if ($active)
            $this->db->where('expire', time(), '>');
        else {
            $this->db->where('expire', time() - 60 * 60 * 24 * 2, '>');
        }
        return $this->db->orderBy('expire')
                        ->where("user_id", $userId)->get($this->tableName);
    }

    public function insertSess($IP, $browser, $remeberMe = true, $type = "normal", $adminId = null) {
        $token = \JR\CORE\helpers\StringUtils::generate_string(230);
        $expire = time() + ($remeberMe ? $this->longExpire : $this->shortExpire);
        $data = ['user_id' => $this->userId,
            'token' => $token,
            'expire' => $expire,
            'type' => $type,
            'admin_id' => $adminId,
            'IP' => $IP,
            'browser' => $browser];
        parent::insert($data);
        return [$token, $expire];
    }

    public function InvalideSession($id) {
        return parent::update($id, ['expire' => 1]);
    }

    public function invalidateAll($user_id = null) {
        parent::update($user_id ? $user_id : $this->userId, ['expire' => 1], "user_id");
    }

    public function insertOrUpdate($data) {
        throw new \Exception("Not possible in this class");
    }

    public static function getUserSession($token, $user, \MysqliDb $db) {
        return $db->where("user_id", $user)
                        ->where("token", $token)
                        ->getOne(self::$tableNameStatic);
    }

    public function destroySession($token) {
        $data = ['expire' => 10];
        return parent::update($token, $data, "token");
    }

}
