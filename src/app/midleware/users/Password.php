<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\users;

/**
 * Description of Password
 *
 * @author jakub
 */
class Password extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "user_internal_id";
    protected $vissible = null;
    protected $editable = null;
    protected $tableName = "users_creditals";
    protected $userID = null;

    public function __construct(\MysqliDb $db, $userID) {
        parent::__construct($db);
        $this->userID = $userID;
    }

    public function getAll($limit = 0, $offset = 0) {
        throw new \Exception("This method is not allowed!");
    }

    public function change($password) {
        $data = ['user_internal_id' => $this->userID,
            "password" => password_hash($password, PASSWORD_DEFAULT)];
        return parent::insertOrUpdate($data);
    }

}
