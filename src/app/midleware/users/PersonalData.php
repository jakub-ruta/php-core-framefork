<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\users;

/**
 * Description of PersonalData
 *
 * @author jakub
 */
class PersonalData extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "user_internal_id";
    protected $vissible = null;
    protected $editable = array('first_name', 'last_name', 'birth_date',
        'country', 'city', 'zip', 'adress', 'company', 'phone', 'gender');
    protected $tableName = "users_personal_informations";
    protected $userId;

    public function __construct(\MysqliDb $db, $userId) {
        parent::__construct($db);
        $this->userId = $userId;
    }

    public function getAll($limit = 1000, $offset = 0): array {
        throw new \Exception("This method is not allowed!");
    }

    /**
     * Get user personal data for current logged user
     * @return array of print in db
     */
    public function getMe() {
        return parent::getOne($this->userId);
    }

    public function insert($data) {
        throw new \Exception("This method is not allowed!");
    }

    public function insertOrUpdate($data) {
        return parent::insertOrUpdate($data);
    }

    /**
     * Update user (me)
     * @param array $data
     * @return id of inserted row
     */
    public function updateMe($data) {
        $data = array_intersect_key($data, array_flip($this->editable));
        $data['user_internal_id'] = $this->userId;
        return parent::insertOrUpdate($data);
    }

}
