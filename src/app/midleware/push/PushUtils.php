<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\push;

/**
 * Description of PushUtils
 *
 * @author jakub
 */
class PushUtils extends \JR\CORE\midleware\Midleware {

    protected $primaryKey = "id";
    protected $tableName = "users_devices";

    /**
     * Add push device id to user
     * @param type $device_id
     * @param type $user_id
     */
    public function addDevice($device_id, $user_id) {
        parent::insertOrUpdate(array("device_id" => $device_id, "user_id" => $user_id));
    }

    public function getUserDevices($user_id) {
        return $this->db->where('user_id', $user_id)->get($this->tableName);
    }

}
