<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\midleware\notifications;

/**
 * Description of Notifications
 *
 * @author jakub
 */
class Notifications extends \JR\CORE\midleware\Midleware {

    /**
     *
     * @var string name of table in db (without prefix)
     */
    protected $tableName = "users_notifications";

    public function getForUser($id) {
        return $this->db->where("n.user_id", $id)
                        ->orderBy("n.new", "DESC")
                        ->orderBy("n.created")
                        ->join("users u", "n.from_user_id=u.internal_id", "LEFT")
                        ->get($this->tableName . " n", null,
                                "n.*, u.login_name, u.avatar");
    }

    /**
     * Mark all notifications as read for user
     * @param int $user_id
     * @param int $time timestamp for cooldown for new created notifications
     */
    public function setRead($user_id, $time) {
        return $this->db->where("user_id", $user_id)
                        ->where("created", date("Y-m-d H:i:s", $time), "<")
                        ->update($this->tableName, ['new' => 0]);
    }

    public function delete($user_id, $notif_id) {
        return $this->db->where("user_id", $user_id)
                        ->where("id", $notif_id)
                        ->delete($this->tableName, 1);
    }

    public function createDepInv($user_id, $admin_id) {
        return parent::insert(['user_id' => $user_id,
                    'type' => 'department_invitation',
                    'from_user_id' => $admin_id,
                    'color' => 'secondary']);
    }

    public function createDepInvAccept($user_id, $admin_id) {
        return parent::insert(['user_id' => $user_id,
                    'type' => 'department_invitation_accept',
                    'from_user_id' => $admin_id,
                    'color' => 'success']);
    }

    public function createDepInvDeceline($user_id, $admin_id) {
        return parent::insert(['user_id' => $user_id,
                    'type' => 'department_invitation_deceline',
                    'from_user_id' => $admin_id,
                    'color' => 'danger']);
    }

}
