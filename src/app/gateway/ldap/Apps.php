<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\gateway\ldap;

/**
 * Description of Apps
 *
 * @author jakub
 */
class Apps {

    protected $apps = null;
    protected $expire = 0;

    /**
     *
     * @var \JR\CORE\gateway\ldap
     */
    protected $ldap;

    /**
     *
     * @var \JR\CORE\midleware\users
     */
    protected $user;

    public function __construct(\JR\CORE\gateway\ldap $ldap = null, \JR\CORE\midleware\users $user = null) {
        $this->ldap = $ldap;
        $this->user = $user;
        $this->prepareApps();
        trigger_error("Apps class is not complete", E_USER_WARNING);
        trigger_error("Apps view do not contain data for apps", E_USER_WARNING);
    }

    public function prepareApps() {
        try {
            $this->loadFromSession();
            $this->state = "from_session";
        } catch (\JR\CORE\gateway\apps\NotInSession $exc) {
            $this->state = "missing";
        }
    }

    public function loadFromSession() {

    }

}
