<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\cookies;

/**
 * Description of Cookies
 *
 * @author jakub
 */
class Cookies {

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;
    protected $sessionName = 'session';

    public function __construct(\JR\CORE\request\request $request) {
        $this->request = $request;
    }

    public function setSession($data) {
        bdump($this->request->getPathPrefix(), "BASE");
        setcookie($this->sessionName, $data[0], $data[1],
                $this->request->getPathPrefix() . "/", $this->request->getDomain());
    }

    public function getUserSession() {
        $cookies = $this->request->getCookies();
        if (isset($cookies[$this->sessionName])) {
            return $cookies[$this->sessionName];
        }
        return null;
    }

    public function destroySession() {
        setcookie($this->sessionName, "", 10,
                $this->request->getPathPrefix() . "/", $this->request->getDomain());
    }

}
