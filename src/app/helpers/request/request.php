<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\request;

/**
 * Description of request
 *
 * @author jakub
 */
class request {

    protected $full_url;
    protected $path;
    protected $parsedPath = array();
    protected $is_https = false;
    protected $post;
    protected $get;
    protected $files;
    protected $cookies;
    protected $pathPrefix;
    protected $userIP;
    protected $userAgent;
    protected $domainWithScheme;
    protected $session;
    protected $domain;

    function __construct() {
        $this->domain = $_SERVER['HTTP_HOST'];
        $this->domainWithScheme = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")
                . "://$this->domain";
        $this->full_url = $this->domainWithScheme . $_SERVER['REQUEST_URI'];
        $this->path = $this->parsePath($this->full_url);
        $this->is_https = parse_url($this->full_url, PHP_URL_SCHEME) == 'https' ? true : false;
        $this->post = ($tmp = filter_input_array(INPUT_POST)) ? $tmp : Array();
        $this->get = ($tmp = filter_input_array(INPUT_GET)) ? $tmp : Array();
        $this->files = ($tmp = $_FILES) ? $tmp : Array();
        $this->cookies = ($tmp = filter_input_array(INPUT_COOKIE)) ? $tmp : Array();
        $this->session = $_SESSION;
        $this->parsedPath = explode("/", $this->path);
        $this->userIP = $_SERVER['REMOTE_ADDR'];
        $this->userAgent = $_SERVER['HTTP_USER_AGENT'];
        \Tracy\Debugger::getBar()->addPanel(new \JR\CORE\bridges\tracy\RequestPanel($this));
    }

    /**
     *
     * @return string full url
     */
    public function getFull_url() {
        return $this->full_url;
    }

    /**
     *
     * @return string requested app path, relative to index.php
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * @param string to strip
     * @return array requested app path, relative to index.php parsed to array expolded by '/'
     */
    public function getParsedPath($strip = null) {
        if ($strip != null) {
            return explode("/", str_replace($strip, "", $this->path));
        }
        return $this->parsedPath;
    }

    /**
     *
     * @return bool Is https been used
     */
    public function getIs_https() {
        return $this->is_https;
    }

    /**
     *
     * @return array parsed post global array
     */
    public function getPost($prefix = null) {
        if ($prefix == null) {
            return $this->post;
        }
        $data = array();
        foreach ($this->post as $key => $value) {
            if (str_starts_with($key, $prefix)) {
                $data[$key] = $value;
            }
        }
        return $data;
    }

    /**
     *
     * @return array parsed global get array
     */
    public function getGet() {
        return $this->get;
    }

    /**
     *
     * @return array parsed global files array
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     *
     * @return array parse global array with cookies
     */
    public function getCookies() {
        return $this->cookies;
    }

    /**
     * For cases that app is not running under core
     * @return string path to index.php file
     */
    public function getPathPrefix() {
        return $this->pathPrefix;
    }

    /**
     *
     * @return string IP adress of user router
     */
    public function getUserIP() {
        return $this->userIP;
    }

    /**
     *
     * @return string user browser agent sended
     */
    public function getUserAgent() {
        return $this->userAgent;
    }

    /**
     * for html tag base url
     * @return string full url path to index.php file with domain (relative to httpd config)
     */
    public function getBasePath() {
        return $this->domainWithScheme . $this->pathPrefix;
    }

    /**
     *
     * @param type $url
     * @return type
     */
    public function parsePath($url) {
        $this->pathPrefix = str_replace("/index.php", "", $_SERVER['SCRIPT_NAME']);
        $parsed_url = rtrim(parse_url($url, PHP_URL_PATH), "/");
        return (ltrim(str_replace($this->pathPrefix, "", $parsed_url), "/"))
        ;
    }

    /**
     *
     * @param string $str path that should be compared
     * @return boolean if path starts with param string
     */
    public function startWith($str) {
        if ($this->path == '' && $str != $this->path) {
            return false;
        }
        return str_starts_with($this->path, $str);
    }

    public function getSession() {
        return $this->session;
    }

    public function getDomain() {

    }

    public function getLimit() {
        if (isset($this->get['limit'])) {
            return $this->get['limit'];
        } else {
            return 1000;
        }
    }

    public function getPage() {
        if (isset($this->get['page'])) {
            return $this->get['page'];
        } else {
            return 0;
        }
    }

    public function getJSONQuery() {
        return json_encode(array('get' => $this->get, 'post' => $this->post));
    }

    public function isGetRequest() {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            return true;
        }
        return false;
    }

}
