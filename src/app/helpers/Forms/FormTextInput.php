<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormTextInput
 *
 * @author jakub
 */
class FormTextInput extends FormControl {

    protected $placeholder = null;
    protected $type = "text";

    public function placeholder($placeholder) {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function render() {
        $html = '';
        if ($this->label)
            $html = $this->renderLabel();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupStart();
        if ($this->prepend)
            $html .= $this->renderPrepend();
        $html .= '<input type="' . $this->type . '" name="' . $this->name
                . '" id="' . $this->id
                . '" class="' . $this->class . " " . $this->margin
                . '" placeholder="' . $this->placeholder
                . '" value="' . ($this->type == "password" ? "" : $this->value) . '" ';
        if ($this->required) {
            $html .= ' required="required" ';
        }
        if ($this->disabledFunc)
            $html .= ' disabled" ';
        if ($this->readonly)
            $html .= ' readonly" ';
        $html .= ($this->disabledFunc ? " disabled " : "" ) . '/>';
        if ($this->append)
            $html .= $this->renderAppend();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupEnd();
        if ($this->text)
            $html .= $this->renderFormText();
        return $html;
    }

    public function evaluate() {
        if (!isset($_REQUEST[$this->name]) && !$this->disabledFunc) {
            bdump($this->name);
            bdump($_REQUEST);
            throw new FormException("missing input");
        }
        if ($this->required) {
            if ($_REQUEST[$this->name] == '') {
                throw new FormException("Control data is required");
            }
        }
    }

}
