<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormHidden
 *
 * @author jakub
 */
class FormHidden extends FormControl {

    public function __construct($name, $value) {
        $this->name = $name;
        $this->value = $value;
        if (isset($_REQUEST[$this->name])) {
            $this->value = $_REQUEST[$this->name];
        }
        $this->id = $name;
        $this->label = null;
    }

    //put your code here
    public function render() {
        return '<input type="hidden" name="' . $this->name
                . '" id="' . $this->id
                . '" value="' . $this->value . '" />';
    }

    public function evaluate() {
        if (!isset($_REQUEST[$this->name])) {
            throw new FormException("missing input");
        }
        return;
    }

}
