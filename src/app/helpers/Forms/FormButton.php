<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormButton
 *
 * @author jakub
 */
class FormButton extends FormControl {

    protected $class = 'form-control btn';

    //put your code here
    public function render() {
        return '<input class="' . $this->class . ' ' . $this->margin . '" '
                . ' type="submit" value="' . $this->value . '" '
                . 'name="' . $this->name . '" '
                . '' . ($this->readonly ? ' readonly="readonly" ' : "") . ''
                . 'id="' . $this->id . '" '
                . ($this->disabledFunc ? " disabled " : "" ) . '>';
    }

    public function __construct($name, $value) {
        $this->name = $name;
        $this->value = $value;
        $this->id = $name;
        $this->label = null;
    }

    public function evaluate() {
        if (!isset($_REQUEST[$this->name])) {
            throw new FormException("missing input");
        }
        return;
    }

}
