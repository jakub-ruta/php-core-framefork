<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\dependencies;

/**
 * Description of DependenciContainer
 *
 * @author jakub
 */
class DependenciContainer {

    /**
     *
     * @var JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected static $_instance = null;

    /**
     *
     * @var array of user defined dependencies
     */
    protected $userDefined = array();

    /**
     *
     * @var \JR\CORE\midleware\users\User
     */
    protected $user;

    /**
     *
     * @var \JR\CORE\midleware\users\User
     */
    protected $admin = null;

    /**
     *
     * @var \JR\CORE\helpers\factories\users\UserFactory
     */
    protected $userFactory;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request = null;

    /**
     *
     * @var \MysqliDb
     */
    protected $db = null;
    protected $curl;

    /**
     *
     * @var \JR\CORE\helpers\session\SessionsUtils
     */
    protected $session;

    /**
     *
     * @var \JR\CORE\helpers\cookies\Cookies
     */
    protected $cookies;

    /**
     *
     * @var \JR\CORE\helpers\langs\Lang
     */
    protected $translations;

    /**
     *
     * @var \JR\CORE\config\Config
     */
    protected $config;

    /**
     *
     * @var \JR\CORE\midleware\log\ActionLog
     */
    protected $actionLog;

    /**
     *
     * @var \JR\CORE\midleware\push\PushUtils
     */
    protected $pushUtils;

    /**
     *
     * @var \JR\CORE\midleware\notifications\Notifications
     */
    protected $notifications = null;

    /**
     *
     * @var \JR\CORE\helpers\mail\EmailTool
     */
    protected $email = null;

    public function __construct(\MysqliDb $db = null, \JR\CORE\request\request $request = null) {
        $this->request = $request;
        $this->db = $db;
        $this->getDB();
        $this->actionLog = new \JR\CORE\midleware\log\ActionLog($this->db, 1, $this->request ? $this->request->getUserIP() : null);
        if ($request != null) {
            $this->userFactory = new \JR\CORE\helpers\factories\users\UserFactory($this);
            $this->actionLog->setAdmin_level($this->user->getRole());
            $this->prepareTransaltions();
        }
    }

    /**
     * ONLY available if makeMeStatic was called before;
     * @return JR\CORE\helpers\dependencies\DependenciContainer
     */
    static function gI() {
        if (self::$_instance == null)
            throw new \Exception("Object has no instance");
        return self::$_instance;
    }

    public function makeMeStatic() {
        self::$_instance = $this;
    }

    public function getUserDefined($name) {
        if (isset($this->userDefined[$name])) {
            return $this->userDefined[$name];
        } else {
            $this->userDefined[$name] = new $name;
            return $this->userDefined[$name];
        }
    }

    public function getUser() {
        if (isset($this->user))
            return $this->user;
        throw new \Exception("User class has not been inited yet");
    }

    public function getRequest() {
        if ($this->request == null) {
            $this->request = new \JR\CORE\request\request();
        }
        return $this->request;
    }

    public function getDB() {
        if ($this->db == null) {
            $this->db = new \MysqliDb($_ENV["DB_HOST"],
                    $_ENV["DB_USER"],
                    $_ENV["DB_PASS"],
                    $_ENV["DB_NAME"]
            );
            if (isset($_ENV["DB_prefix"])) {
                $this->db->setPrefix($_ENV["DB_prefix"]);
            }
            $this->db->setTrace(true);
            \Tracy\Debugger::getBar()->addPanel(
                    new \JR\CORE\bridges\tracy\DatabasePanel($this->db));
        }
        return $this->db;
    }

    public function getCurl() {
        throw new Exception("CURL was not registered");
    }

    public function getSession() {
        if (isset($this->session))
            return $this->session;
        else {
            $this->session = new \JR\CORE\helpers\session\SessionsUtils($this->request);
            \Tracy\Debugger::getBar()->addPanel(
                    new \JR\CORE\bridges\tracy\SessionPanel($this->session));
            return $this->session;
        }
    }

    public function getConfig() {
        if (isset($this->config)) {
            return $this->config;
        } else {
            $this->config = new \JR\CORE\config\Config();
            return $this->config;
        }
    }

    public function getUserFactory(): \JR\CORE\helpers\factories\users\UserFactory {
        if (isset($this->userFactory)) {
            return $this->userFactory;
        } else {
            $this->userFactory = new \JR\CORE\helpers\factories\users\UserFactory($this);
            return $this->userFactory;
        }
    }

    public function getAdmin() {
        return $this->admin;
    }

    public function setUser(\JR\CORE\midleware\users\User $user) {
        $this->user = $user;
        $this->actionLog->setData($user->getInternalId(),
                $this->request->getUserIP(),
                $this->cookies->getUserSession(),
                ($this->admin != null ? "admin" : "normal"),
                ($this->admin != null ? $this->admin->getInternalId() : null));
    }

    public function getCookies() {
        if (isset($this->cookies)) {
            return $this->cookies;
        } else {
            $this->cookies = new \JR\CORE\helpers\cookies\Cookies($this->request);
            return $this->cookies;
        }
    }

    public function setAdmin($user) {
        $this->admin = $user;
    }

    public function getTranslations() {
        return $this->translations;
    }

    public function getActionLog(): \JR\CORE\midleware\log\ActionLog {
        return $this->actionLog;
    }

    /**
     *
     * @return \JR\CORE\midleware\push\PushUtils
     */
    public function getPushUtils() {
        if (isset($this->pushUtils)) {
            return $this->pushUtils;
        } else {
            $this->pushUtils = new \JR\CORE\midleware\push\PushUtils($this->db);
            return $this->pushUtils;
        }
    }

    /**
     *
     * @return \JR\CORE\midleware\notifications\Notifications
     */
    public function getNotifications() {
        if (isset($this->notifications)) {
            return $this->notifications;
        } else {
            $this->notifications = new \JR\CORE\midleware\notifications\Notifications($this->db);
            return $this->notifications;
        }
    }

    /**
     *
     * @return \JR\CORE\helpers\mail\EmailTool
     */
    public function getEmailTool() {
        if (isset($this->email)) {
            return $this->email;
        } else {
            $this->email = new \JR\CORE\helpers\mail\EmailTool($this->db);
            return $this->email;
        }
    }

    /**
     * Function for init translations
     */
    protected function prepareTransaltions() {
        $this->translations = new \JR\CORE\helpers\langs\Lang();
        $this->translations->makeMeStatic();
        if (isset($this->user)) {
            $this->translations->setPrefered($this->user->getLang());
        }
        if ($this->getSession()->getPreferedLang()) {
            $this->translations->setPrefered($this->getSession()->getPreferedLang());
        }
    }

}
