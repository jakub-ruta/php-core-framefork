<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormSelect
 *
 * @author jakub
 */
class FormSelect extends FormControl {

    protected $options = array();
    protected $hidden = array();
    protected $disabled = array();
    protected $onchange = null;
    protected $vissible = array();

    public function setOptions($options, $key = null, $value = null) {
        if ($key != null) {
            foreach ($options as $opt) {
                $this->options[$opt[$key]] = $opt[$value];
            }
        } else {
            $this->options = $options;
        }
        return $this;
    }

    public function setSelected($selected, $override = false) {
        $this->value($selected, $override);
        return $this;
    }

    public function setOnChange($action) {
        $this->onchange = $action;
        return $this;
    }

    /**
     * For disable some options
     * @param array $disabled array of keys
     * @return $this
     */
    public function setDisabled($disabled) {
        $this->disabled = $disabled;
        return $this;
    }

    /**
     * For hide some values unless checked
     * @param array $hidden array of keys
     * @return $this
     */
    public function setHidden($hidden) {
        $this->hidden = $hidden;
        return $this;
    }

    public function setVissible($vissible, $key = null, $value = null) {
        if ($key != null) {
            foreach ($vissible as $vis) {
                $this->vissible[$vis[$key]] = $vis[$value];
            }
        } else {
            $this->vissible = $vissible;
        }
        return $this;
    }

    public function render() {
        if (count($this->vissible) > 0)
            $this->convertVissible();
        $html = '';
        if ($this->label)
            $html = $this->renderLabel();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupStart();
        if ($this->prepend)
            $html .= $this->renderPrepend();
        $html .= $this->renderSelectStart();
        foreach ($this->options as $key => $value) {
            $html .= $this->renderOption($key, $value);
        }
        $html .= $this->renderSelectEnd();
        if ($this->append)
            $html .= $this->renderAppend();
        if ($this->prepend || $this->append)
            $html .= $this->renderInputGroupEnd();
        if ($this->text)
            $html .= $this->renderFormText();
        return $html;
    }

    protected function renderOption($key, $value) {
        $html = '';
        $hidden = (in_array($key, $this->hidden) ? true : false);
        $disabled = (in_array($key, $this->disabled) ? true : false);
        $selected = ($key == $this->value ? true : false);
        if (!$hidden || $selected) {
            $html .= '<option value="' . $key . '"';
            if ($selected)
                $html .= ' selected="selected" ';
            if ($disabled)
                $html .= 'disabled="disabled" ';
            $html .= '>' . ucfirst($value);
            $html .= '</option>';
        }
        return $html;
    }

    protected function renderSelectStart() {
        return '<select class="' . $this->class . ' ' . $this->margin . '" '
                . 'id="' . $this->id . '" '
                . '' . ($this->required ? ' required="required" ' : "") . ''
                . '' . ($this->readonly ? ' readonly="readonly" ' : "") . ''
                . '' . ($this->onchange ? 'onchange="' . $this->onchange . '"' : "") . ''
                . '' . ($this->name ? 'name="' . $this->name . '"' : "") . ''
                . ($this->disabledFunc ? " disabled " : "" ) . '>';
    }

    protected function renderSelectEnd() {
        return '
            </select>
            ';
    }

    public function convertVissible() {
        foreach ($this->options as $key => $value) {
            if ($this->vissible[$key]) {
                continue;
            } else {
                $this->hidden[] = $key;
            }
        }
        bdump($this, "render");
    }

    public function evaluate() {
        if (!isset($_REQUEST[$this->name]) && $this->required) {
            throw new FormException("missing input");
        }
        if (!array_key_exists($_REQUEST[$this->name], $this->options) && $this->required) {
            throw new FormException("Option is not from options");
        }
    }

}
