<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 *
 * @author jakub
 */
abstract class FormControl {

    protected $required = false;
    protected $name;
    protected $id;
    protected $onclick = null;
    protected $label;
    protected $class = 'form-control';
    protected $prepend = null;
    protected $append = null;
    protected $value = null;
    protected $margin = "my-10";
    protected $text = null;

    /**
     * If control should render as disabled
     * @var bool
     */
    protected $disabledFunc = false;
    protected $readonly = false;

    public function getName() {
        return $this->name;
    }

    public function __construct($name, $label) {
        $this->name = $name;
        $this->id = $name;
        $this->label = $label;
        if (isset($_REQUEST[$this->name])) {
            $this->value = $_REQUEST[$this->name];
        }
    }

    public function setText($text): void {
        $this->text = $text;
    }

    public function setDisabledFunc($disabled = true): void {
        $this->disabledFunc = $disabled;
    }

    public function setPrepend($prepend) {
        $this->prepend = $prepend;
        return $this;
    }

    public function setMargin($m) {
        $this->margin = $m;
        return $this;
    }

    public function setAppend($append) {
        $this->append = $append;
        return $this;
    }

    public function required($param = true) {
        $this->required = $param;
        return $this;
    }

    public function value($param = true, $override = false) {
        if ($this->value == null || $this->value == '' || $override) {
            $this->value = $param;
        }
        return $this;
    }

    public function id($name) {
        $this->id = $name;
        return $this;
    }

    public function onClick($param) {
        $this->onclick = $param;
        return $this;
    }

    public function Class($class) {
        $this->class = $class;
        return $this;
    }

    abstract public function render();

    protected function renderLabel() {
        return '
                <label for="' . $this->id . '" '
                . 'class="' . ($this->required ? " required " : "") . '"'
                . '><b>' . $this->label . ':</b></label>';
    }

    protected function renderInputGroupStart() {
        return ' <div class="input-group">';
    }

    protected function renderInputGroupEnd() {
        return ' </div>';
    }

    protected function renderFormText() {
        return ' <div class="form-text">
      ' . $this->text . '
    </div>';
    }

    protected function renderPrepend() {
        return '<div class="input-group-prepend ' . $this->margin . '">
        <span class="input-group-text">' . $this->prepend . ' </span>
      </div>';
    }

    protected function renderAppend() {
        return '<div class="input-group-append ' . $this->margin . '">
        <span class="input-group-text">' . $this->append . ' </span>
      </div>';
    }

    public function getValue() {
        return $this->value;
    }

    public function readonly() {
        $this->readonly = true;
        return $this;
    }

    abstract function evaluate();
}
