<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\form;

/**
 * Description of FormFactory
 *
 * @author jakub
 */
class FormFactory {

    /**
     *
     * @var class FormControl
     */
    protected $controls = array();

    /**
     *
     * @var strin Name of form
     */
    protected $form_name;

    /**
     *
     * @var string Form method GET/POST
     */
    protected $method = "POST";

    /**
     *
     * @var string defeult action for form
     */
    protected $action = null;

    /**
     *
     * @var string class of form form/form-inline
     */
    protected $form_class = "form";

    /**
     *
     * @var bool is form sended?
     */
    protected $sended = false;

    /**
     *
     * @param string $form_name
     * @return FormFactory
     */

    /**
     *
     * @var string CSRF token
     */
    protected $csrf;

    public function __construct($form_name, $csrf) {
        $this->form_name = $form_name;
        $this->csrf = $csrf;
        $this->createHidden("csrf", "test")->value($csrf, true);
        $this->createHidden("form-name", $form_name)->value($form_name, true);
        if (isset($_REQUEST['csrf']) && isset($_REQUEST['form-name']) && $form_name == $_REQUEST['form-name']) {
            $this->sended = true;
        }

        return $this;
    }

    public function getForm_name() {
        return $this->form_name;
    }

    public function getMethod() {
        return $this->method;
    }

    public function getAction() {
        return $this->action;
    }

    public function getForm_class() {
        return $this->form_class;
    }

    /**
     * Get one already created control
     * @param strin $name
     * @return FormControl
     */
    public function getControl($name) {
        return $this->controls[$name];
    }

    public function setForm_name($form_name) {
        $this->form_name = $form_name;
        return $this;
    }

    public function setMethod($method) {
        $this->method = $method;
        return $this;
    }

    public function setAction($action) {
        $this->action = $action;
        return $this;
    }

    /**
     *
     * @param type $form_class
     * @return $this
     */
    public function setForm_class($form_class) {
        $this->form_class = $form_class;
        return $this;
    }

    /**
     *
     * @param string $name
     * @return FormSelect
     */
    public function createSelect($name, $label) {
        return $this->controls[$name] = new FormSelect($name, $label);
    }

    /**
     *
     * @param string $name
     * @return FormTextArea
     */
    public function createTextArea($name, $label) {
        return $this->controls[$name] = new FormTextArea($name, $label);
    }

    /**
     *
     * @param string $name
     * @return FormBUtton
     */
    public function createButton($name, $value) {
        return $this->controls[$name] = new FormButton($name, $value);
    }

    /**
     *
     * @param string $name
     * @return FormCheckBox
     */
    public function createCheckBox($name, $label) {
        return $this->controls[$name] = new FormCheckBox($name, $label);
    }

    /**
     *
     * @param type $name
     * @param type $value
     * @return FormHidden
     */
    public function createHidden($name, $value) {
        return $this->controls[$name] = new FormHidden($name, $value);
    }

    /**
     *
     * @param type $name
     * @param type $label
     * @return FormNumberInput
     */
    public function createNumberInput($name, $label) {
        return $this->controls[$name] = new FormNumberInput($name, $label);
    }

    /**
     *
     * @param string $name
     * @param string $label
     * @return FormTextInput
     */
    public function createTextInput($name, $label) {
        return $this->controls[$name] = new FormTextInput($name, $label);
    }

    /**
     * Render all form
     * @return string HTML code
     */
    public function renderAll() {
        $html = $this->renderFormStart();
        foreach ($this->controls as $cont) {
            $html .= $cont->render();
        }
        $html .= $this->rederFormEnd();
        return $html;
    }

    /**
     * Render start of form, without any control
     * @return string
     */
    protected function renderFormStart() {
        $html = '<form method="' . $this->method . '" '
                . 'class="' . $this->form_class . '" '
                . 'name="' . $this->form_name . '" '
                . 'id="' . $this->form_name . '" ';
        if ($this->action) {
            $html .= 'action="' . $this->action . '" ';
        }
        $html .= ">";
        return $html;
    }

    /**
     * Render form end
     * @return string
     */
    public function rederFormEnd() {
        return '</form>';
    }

    public function getValues() {
        $data = array();
        foreach ($this->controls as $key => $value) {
            if ($key == 'csrf') {
                if ($this->sended) {
                    if ($_REQUEST['csrf'] != $this->csrf) {
                        throw new CSRFViolation();
                    }
                }
            } elseif ($value instanceof FormButton) {
                continue;
            } else {
                $data[$value->getName()] = $value->getValue();
            }
        }
        return $data;
    }

    public function isSend() {
        if ($this->sended) {
            $this->evaluate();
            return true;
        }
        return false;
    }

    public function evaluate() {
        foreach ($this->controls as $key => $value) {
            $value->evaluate();
        }
    }

    public function setDisabled($names = array()) {
        if (count($names) == 0) {
            foreach ($this->controls as $value) {
                $value->setDisabledFunc();
            }
        } else {
            foreach ($names as $value) {
                $this->controls[$value]->setDisabledFunc();
            }
        }
    }

    public function vulues($data, $override = false) {
        foreach ($data as $key => $value) {
            if ($this->controls[$key]) {
                $this->controls[$key]->value($value, $override);
            }
        }
    }

}
