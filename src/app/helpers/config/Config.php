<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\config;

/**
 * Description of Config
 *
 * @author jakub
 */
class Config {

    protected $global = array();
    protected $database = array();
    protected $config = array("DB" => array("InfoBar" => null));
    protected $version = "unknown";
    protected $core_version = "unknown";

    public function __construct() {
        $this->loadVersion();
    }

    public function loadVersion() {
        if (file_exists("version")) {
            $this->version = file_get_contents("version");
        }
        if (file_exists(__DIR__ . "/../../../../version")) {
            $this->core_version = file_get_contents(__DIR__ . "/../../../../version");
        }
    }

    public function getVersion() {
        return $this->version;
    }

    public function getCoreVersion() {
        return $this->core_version;
    }

    public function getTitle() {
        return $_ENV["APP_NAME"];
    }

    public function get($key, $cat = "ENV") {
        if ($cat == "ENV") {
            return $_ENV[$key];
        }
        return $this->config[$cat][$key];
    }

    public function getBool($key, $cat = "ENV") {
        throw new \Exception("This part of code doesn´t work and still return false even if it is true");
        if ($cat == "ENV") {
            return boolval($_ENV[$key]);
        }
        return boolval($this->config[$cat][$key]);
    }

    public function getLogo() {
        return $_ENV["APP_LOGO"];
    }

}
