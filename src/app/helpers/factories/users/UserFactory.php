<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\helpers\factories\users;

/**
 * Description of UserFactory
 *
 * @author jakub
 */
class UserFactory extends \JR\CORE\helpers\factories\Factory {

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        parent::__construct($dep);
        $this->tryLoginViaCookie();
    }

    public function registerUser($name, $email, $password, $departements = array(), $dev = false) {
        $id = $this->dep->getUser()->insert(["login_name" => $name,
            "mail" => $email, 'nickname' => ucfirst($name), 'main_role' => $dev ? "10" : "0"]);
        $this->dep->getUser()->password->insert(['user_internal_id' => $id,
            "password" => password_hash($password, PASSWORD_DEFAULT)]);
        $this->dep->getUser()->rights->repairRights($id);
        if (count($departements) > 0) {
            $user = new \JR\CORE\midleware\users\User($id, $this->dep->getDB());
            foreach ($departements as $key => $value) {
                $user->departments->addDepartment($key, $value);
            }
        }
        return $id;
    }

    public function tryLogin($session, $request) {
        return new \JR\CORE\midleware\users\User(1, $this->dep->getDB());
    }

    public function tryLocalLogin($userName, $password, $remember_me) {
        $userMidleware = $this->dep->getUser();
        $user = $userMidleware->getOne($userName, "LOWER(login_name)");
        if ($user == null || $user['internal_id'] < 10) {
            throw new LocalLoginException();
        }
        if ($user['banned'] == 1) {
            throw new AccountBannedException();
        }
        if ($user['disabled'] == 1) {
            throw new AccountDissabledException();
        }
        $pass = $userMidleware->password->getOne($user['internal_id'], "user_internal_id");
        switch ($pass['type']) {
            case 'jr_hash':
                if (password_verify($password, $pass['password'])) {
                    $this->loginUser($user['internal_id'], $remember_me);
                    return;
                }
                throw new LocalLoginException();
                break;
            default:
                throw new LocalLoginException();
        }
    }

    public function loginUser($user_id, $rememberMe = true, $type = 'normal', $adminId = null) {
        $user = new \JR\CORE\midleware\users\User($user_id, $this->dep->getDB());
        $data = $user->userSession->insertSess($this->dep->getRequest()->getUserIP(),
                $this->dep->getRequest()->getUserAgent(),
                $rememberMe, $type, $adminId);
        $data[0] = $user->getInternalId() . '$' . $data[0];
        $this->dep->getCookies()->setSession($data);
        $this->dep->setUser($user);
    }

    protected function tryLoginViaCookie() {
        $cookie = $this->dep->getCookies()->getUserSession();
        if ($cookie) {
            $cookie = explode("$", $cookie);
            $data = \JR\CORE\midleware\sessions\Session::getUserSession($cookie[1],
                            $cookie[0], $this->dep->getDB());
            if (isset($data['type']) && $data['expire'] > time() + 10) {
                if ($data['type'] == 'admin') {
                    $this->dep->setAdmin(new \JR\CORE\midleware\users\User($data['admin_id'], $this->dep->getDB()));
                }
                $this->dep->setUser(new \JR\CORE\midleware\users\User($data['user_id'], $this->dep->getDB()));
                return true;
            } else {
                $this->dep->getCookies()->destroySession();
            }
        }
        $this->dep->setUser(new \JR\CORE\midleware\users\User(1, $this->dep->getDB()));
    }

    public function logoutUser() {
        $cookie = $this->dep->getCookies()->getUserSession();
        $cookie = explode("$", $cookie);
        $this->dep->getUser()->userSession->destroySession($cookie[1]);
        $this->dep->getCookies()->destroySession();
    }

    public function getUserByLoginName($login_name) {
        return \JR\CORE\midleware\users\User::getUserByLoginName($login_name, $this->dep->getDB());
    }

}
