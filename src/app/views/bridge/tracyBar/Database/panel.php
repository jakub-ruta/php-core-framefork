<h1>Queries: <?= $data['count'] ?> </h1>

<div class="tracy-inner">
    <div class="tracy-inner-container">
        <table>
            <thead>
                <tr>
                    <th>Time</th>
                    <th>Query</th>
                    <th>Stack</th>
                </tr>
            </thead>
            <?php foreach ($data['queries'] as $key => $value): ?>
                <tr>
                    <th>
                        <?= round($value[1] * 1000, 4) ?> ms
                    </th>
                    <td>
                        <?= $value[0] ?>
                    </td>
                    <td>
                        <?= $value[2] ?>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>