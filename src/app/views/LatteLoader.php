<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\views;

/**
 * Description of LatteLoader
 *
 * @author jakub
 */
class LatteLoader implements \Latte\Loader {

    protected $coreViews = __DIR__ . '/views/';
    protected $appViews = 'app/views/';

    /**
     * Returns template source code.
     * @param  string  $name
     * @return string
     */
    function getContent($name) {
        if (file_exists($this->appViews . $name)) {
            return file_get_contents($this->appViews . $name);
        } else if (file_exists($this->coreViews . $name)) {
            return file_get_contents($this->coreViews . $name);
        } else if (file_exists($name)) {
            return file_get_contents($name);
        } else {
            throw new \Latte\RuntimeException("Missing template file '$name'.");
        }
    }

    /**
     * Checks whether template is expired.
     * @param  string  $name
     * @param  int  $time
     * @return bool
     */
    function isExpired($name, $time) {
        if (file_exists($this->appViews . $name)) {
            $file = $this->appViews . $name;
        } else if (file_exists($this->coreViews . $name)) {
            $file = $this->coreViews . $name;
        } else if (file_exists($name)) {
            $file = $name;
        } else {
            throw new \Latte\RuntimeException("Missing template file '$name'.");
        }
        $mtime = @filemtime($file); // @ - stat may fail
        return !$mtime || $mtime > $time;
    }

    /**
     * Returns referred template name.
     * @param  string  $name
     * @param  string  $referringName
     * @return string
     */
    function getReferredName($name, $referringName) {
        if (file_exists($this->appViews . $name)) {
            return $this->appViews . $name;
        } else if (file_exists($this->coreViews . $name)) {
            return $this->coreViews . $name;
        } else {
            throw new \Latte\RuntimeException("Missing template file '$name'.");
        }
    }

    /**
     * Returns unique identifier for caching.
     * @param  string  $name
     * @return string
     */
    function getUniqueId($name) {
        if (file_exists($this->appViews . $name)) {
            return str_replace("/", "-", $this->appViews . $name);
        } else if (file_exists($this->coreViews . $name)) {
            return str_replace("/", "-", $this->coreViews . $name);
        } else if (file_exists($name)) {
            return str_replace("/", "-", $name);
        } else {
            throw new \Latte\RuntimeException("Missing template file '$name'.");
        }
    }

}
