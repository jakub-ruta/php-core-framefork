<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\views;

use Latte;
use Tracy\Debugger;

/**
 * Description of View
 *
 * @author jakub
 */
class LatteView {

    protected $data;
    protected $view;

    /**
     *
     * @var class Latte\Engine
     */
    protected $latte;

    public function __construct($data, $view) {

        $this->data = $data;
        $this->view = str_replace(".", "/", $view) . ".latte";
        $this->latte = new \Latte\Engine();
// REQUIRE nette/utils pack
        Debugger::getBar()->addPanel(new \Latte\Bridges\Tracy\LattePanel($this->latte));
        $this->latte->setTempDirectory('app/temp/views');
        $this->latte->setLoader(new LatteLoader());
        $this->latte->addFilter('translate', function ($original, $vars = array()) {
            return \JR\CORE\helpers\langs\Lang::gI()->str($original, $vars);
        });
        $this->latte->addFilter('timeAgo', function ($time) {
            return \JR\CORE\helpers\StringUtils::time_ago($time);
        });
        $this->latte->addFilter('md5', function ($str) {
            return md5($str);
        });
    }

    public function render() {
        $this->latte->render($this->view, $this->data);
    }

    public function renderToString() {
        return $this->latte->renderToString($this->view, $this->data);
    }

}
