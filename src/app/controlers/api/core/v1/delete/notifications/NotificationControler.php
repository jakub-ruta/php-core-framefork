<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\api\core\v1\delete;

/**
 * Description of NotificationsControler
 *
 * @author jakub
 */
class NotificationControler extends \JR\CORE\controler\APIControler {

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute() {
        if (isset($this->request->getPost()["notif_id"])) {
            $this->dep->getNotifications()->delete($this->dep->getUser()->getInternalId(), $this->request->getPost()["notif_id"]);
            $this->msg_success[] = "Notification deleted";
        } else
            $this->msg_error[] = "Unsupported request!";
    }

}
