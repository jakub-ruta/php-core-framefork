<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\api\core\v1\set;

/**
 * Description of NotificationsControler
 *
 * @author jakub
 */
class NotificationsControler extends \JR\CORE\controler\APIControler {

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath("api/core/v1/set/notifications/")[0]) {
            case 'read':
                $this->dep->getNotifications()->setRead($this->dep->getUser()->getInternalId(), time() - 5);
                $this->msg_success[] = "Notification marked as read";
                break;
            default:
                $this->msg_error[] = "Unsupported request!";
                break;
        }
    }

}
