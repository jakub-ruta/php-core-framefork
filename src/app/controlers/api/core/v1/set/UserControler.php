<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\api\core\v1\set;

/**
 * Description of UserControler
 *
 * @author jakub
 */
class UserControler extends \JR\CORE\controler\APIControler {

    /**
     *
     * @var \JR\CORE\controler\APIControler
     */
    protected $next = null;

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath()[5]) {
            case 'push_device':
                $this->next = new user\PushDevice($this->dep);
                break;
            case 'webhooks':
                $this->next = new webhooks\WebHooks($this->dep);
                break;
        }
        if (isset($this->next)) {
            $this->next->execute();
            $this->data = $this->next->data;
            $this->msg_error = $this->next->msg_error;
            $this->msg_warning = $this->next->msg_warning;
            $this->msg_success = $this->next->msg_success;
        }
    }

}
