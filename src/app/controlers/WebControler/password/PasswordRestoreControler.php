<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\password;

use JR\CORE\router\RedirectToError;

/**
 * Description of PasswordControler
 *
 * @author jakub
 */
class PasswordRestoreControler extends \JR\CORE\controler\WebControler {

    public $view = 'core.password.password-restore';
    private $token_id;

    public function execute() {
        if (!isset($this->request->getParsedPath()[1]) &&
                !isset($this->request->getParsedPath()[2])) {
            throw new RedirectToError(422, "Malformed request");
        }
        $id = $this->request->getParsedPath()[1];
        $token = $this->request->getParsedPath()[2];
        $user = $this->getUserByToken($id, $token);
        $this->data['pass_form'] = $this->createPasswordChangeForm($user);
        $this->data['hide_sidebar'] = true;
        $this->data['user_name'] = $user->getLoginName();
    }

    public function createPasswordChangeForm(\JR\CORE\midleware\users\User $user) {
        $form = new \JR\CORE\helpers\form\FormFactory("restore-password", $this->getCSRF());
        $form->createTextInput("password", "Password")
                ->setType("password");
        $form->createTextInput("password-again", "Password confirm")
                ->setType("password");
        $form->createButton("change",
                "Set password");
        if ($form->isSend()) {
            $this->changePassword($form->getValues(), $user);
        }
        return $form;
    }

    public function getUserByToken($id, $token): \JR\CORE\midleware\users\User {
        if (!is_numeric($id) || $id < 11) {
            throw new RedirectToError("401", "Unauthorized request");
        }
        try {
            $user = new \JR\CORE\midleware\users\User($id, $this->db);
            $this->token_id = $user->getTokens()->checkToken($token, 'password-reset')[0];
            return $user;
        } catch (\JR\CORE\midleware\users\UserNotExist $ex) {
            throw new RedirectToError("401", "Unauthorized request");
        } catch (\JR\CORE\midleware\tokens\TokenExpired $ex) {
            throw new RedirectToError("401", "Token expired");
        } catch (\JR\CORE\midleware\tokens\TokenNotBelongsToUser $ex) {
            throw new RedirectToError("401", "Wrong token");
        } catch (\JR\CORE\midleware\tokens\WrongTokenType $ex) {
            throw new RedirectToError("401", "Wrong token type");
        }
    }

    public function changePassword($data, \JR\CORE\midleware\users\User $user) {
        if ($data['password'] != $data['password-again']) {
            $this->setMessage("Password do not match", "danger");
            return;
        }
        if (strlen($data['password']) < 6) {
            $this->setMessage("Password must be at least 6 characters long", "danger");
            return;
        }
        $user->getTokens()->InvalideToken($this->token_id);
        $user->password->change($data['password']);
        $a_log = $this->dep->getActionLog();
        $a_log->setUser_id_for_next_add_log($user->getInternalId());
        $a_log->addLog("Password-restore", "change", null, 1, true);
        $this->setMessage("Your password has been restored! You can now login");
        throw new \JR\CORE\router\RedirectError("login");
    }

}
