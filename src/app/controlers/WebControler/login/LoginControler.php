<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\login;

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class LoginControler extends \JR\CORE\controler\WebControler {

    //put your code here
    public function execute() {
        $new = null;
        switch ($this->request->getParsedPath()[1]) {
            case 'local':
                if (($this->dep->getConfig()->get('local_login')) != 'true') {
                    throw new \JR\CORE\router\RedirectToError(403, "Login locally is not allowed!");
                }
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                $new = new LocalLogin($this->dep);
                break;
            case 'hidden_local':
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                $new = new LocalLogin($this->dep);
                $new->data['hiden_login'] = true;
                break;
            case 'ldap':
                if (($this->dep->getConfig()->get('ldap_login')) != 'true') {
                    throw new \JR\CORE\router\RedirectToError(403, "Login via LDAP is not allowed!");
                }
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                throw new \JR\CORE\router\RedirectToError(501, "LDAP login is not implemented");
                break;
            case 'google':
                if (($this->dep->getConfig()->get('google_login')) != 'true') {
                    throw new \JR\CORE\router\RedirectToError(403, "Login via google is not allowed!");
                }
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                throw new \JR\CORE\router\RedirectToError(501, "Google login is not implemented");
                break;
            case 'admin':
                $new = new AdminLogin($this->dep);
                break;
            default:
                if ($this->dep->getUser()->isLoggedIn()) {
                    throw new \JR\CORE\router\RedirectError("dashBoard");
                }
                break;
        }
        if ($new != null) {
            $new->execute();
            $this->data = $new->data;
        }
        $this->view = 'core.login.login';
        $this->data['hide_sidebar'] = true;
    }

}
