<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\login;

/**
 * Description of AdminLogin
 *
 * @author jakub
 */
class AdminLogin extends \JR\CORE\controler\WebControler {

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = -10;

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = null;

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'admin_login';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = false;

    public function execute() {
        $request = $this->request->getParsedPath("login/admin/");
        if ($request[0] == "logback") {
            $this->logBack();
        }
        if ($this->dep->getUser()->getRole() < 9) {
            throw new \JR\CORE\router\RedirectToError(401, "You do not have required admin level for this action!");
        }
        $this->dep->getUser()->rights->can("users", "admin_login");
        if (!isset($request[0]) || !is_numeric($request[0])) {
            $this->dep->getSession()->setMessages("Wrong parameter!", "danger");
            throw new \JR\CORE\router\RedirectToError(400, "Wrong parameter!");
        }
        if ($this->dep->getAdmin() != null) {
            $this->dep->getSession()->setMessages("You are currently using admin login, you can not use it twice!", "danger");
            throw new \JR\CORE\router\RedirectToError(403, "You are already usding adminlogin!");
        }
        $my_user_id = $this->dep->getUser()->getInternalId();
        $this->dep->getActionLog()->addLog("users", "admin_login", json_encode(
                        ['user_id' => $request[0]]), 5);
        $this->dep->getUserFactory()->loginUser($request[0], false, "admin",
                $this->dep->getUser()->getInternalId());
        $this->makeLogAndLeave("Admin login activated",
                "login", "admin_login", ["admin_id" => $my_user_id],
                "dashBoard", 9, "primary");
    }

    public function logBack() {
        if ($this->dep->getAdmin() == null) {
            $this->dep->getSession()->setMessages("Admin login is not active!", "danger");
            throw new \JR\CORE\router\RedirectToError(403, "Adminlogin is not active!");
        }
        $user_id = $this->dep->getUser()->getInternalId();
        $this->dep->getActionLog()->addLog("login", "admin_login_end", json_encode(
                        ['admin_id' => $this->dep->getAdmin()->getInternalId()]), 5);
        $this->dep->getUserFactory()->loginUser($this->dep->getAdmin()->getInternalId());
        $this->makeLogAndLeave("Admin login disabled",
                "login", "admin_login_end", ["admin_id" => $user_id],
                "admin/users/" . $user_id, 9, "primary");
    }

}
