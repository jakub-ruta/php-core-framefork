<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\user;

/**
 * Description of Profile
 *
 * @author jakub
 */
class Profile extends \JR\CORE\controler\WebControler {

    public $view = "core.users.profile";

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var \JR\CORE\midleware\departments\DeparmentsInvitations
     */
    protected $invitations;

    public function execute() {
        $user = $this->dep->getUser();
        $this->router($user);
        $this->prepareData($user);
    }

    protected function router(\JR\CORE\midleware\users\User $user) {
        $request = $this->request->getParsedPath("user/profile/");
        switch ($request[0]) {
            case 'kill-session':
                $this->killSession($request[1], $user->userSession);
                break;
            case 'logout-everywhere':
                $this->logoutEveryWhere($user);
                break;
            case 'change-password':
                $this->changePassword($user);
                break;
        }
    }

    protected function prepareData(\JR\CORE\midleware\users\User $user) {
        $this->invitations = new \JR\CORE\midleware\departments\DeparmentsInvitations($this->db);
        $csrf = $this->dep->getSession()->getCSRFToken();
        $this->data['admin_levels'] = $user->admin_levels;
        $this->data['user'] = $user->get();
        $form_user = $this->generateUserForm($user->get(), $csrf);
        $this->data['user_form'] = $form_user;
        if ($user->personalInfo) {
            $this->data['user_data_form'] = $this->generatePersonalData($user->personalInfo, $csrf);
        }
        $this->data['sessions'] = $user->userSession->getAll(null, true);
        $this->data['rights'] = $user->rights->getUserRights($this->data['user']['main_role']);
        $this->data['departments'] = $user->departments->get();
        $this->data['invitations'] = $this->invitations->getAllForUser($user->getInternalId());
        $this->data['invitation_form'] = $this->generateInvitationForm($csrf);
        $this->getHeads($user);
        $this->data['push_devices'] = $this->dep->getPushUtils()->getUserDevices($user->getInternalId());
    }

    public function generateUserForm($user, $csrf) {
        $form = new \JR\CORE\helpers\form\FormFactory("user", $csrf);
        $form->createTextInput('login_name', 'Login name')
                ->setPrepend('<i class="fas fa-user" aria-hidden="true"></i>')
                ->value($user['login_name'])
                ->setDisabledFunc();
        $form->createTextInput('nickname', "Nickname")
                ->value($user['nickname'])
                ->setPrepend('<i class="fas fa-id-badge"></i>');
        $form->createTextInput('mail', "E-mail")
                ->required()
                ->value($user['mail'])
                ->setPrepend('<i class="fas fa-at" aria-hidden="true"></i>');
        $form->createSelect('locale', 'Prefered language')
                ->required()
                ->setOptions($this->dep->getTranslations()->getLangs())
                ->value($user['locale'])
                ->setPrepend('<i class="fas fa-language"></i>');
        $form->createTextInput('avatar', "Avatar URL")
                ->value($user['avatar'])
                ->setPrepend('<i class="far fa-user-circle"></i>');
        $form->createButton("user", 'Save')
                ->Class('form-control btn btn-sm btn-success');
        if ($user['origin'] != 'local') {
            $form->setDisabled();
        }
        if ($form->isSend()) {
            $this->ProccedUserForm($form);
        }
        return $form;
    }

    public function generatePersonalData(\JR\CORE\midleware\users\PersonalData $data, $csrf) {
        $form = new \JR\CORE\helpers\form\FormFactory("user_data", $csrf);
        $form->createTextInput('first_name', "Name")
                ->setPrepend('<i class="fas fa-id-card"></i>');
        $form->createTextInput('last_name', 'Last name')
                ->setPrepend('<i class="fas fa-id-card"></i>');
        $form->createTextInput('birth_date', 'Birth date')
                ->setType("date")
                ->setPrepend('<i class="fas fa-birthday-cake"></i>');
        $form->createTextInput('adress', 'Adress')
                ->setPrepend('<i class="fas fa-map-marked-alt"></i>');
        $form->createTextInput('city', 'City')
                ->setPrepend('<i class="fas fa-city"></i>');
        $form->createTextInput("zip", "Post code")
                ->setPrepend('<i class="fas fa-map-marker-alt"></i>');
        $form->createTextInput('country', 'Country')
                ->setPrepend('<i class="fas fa-globe-europe"></i>');
        $form->createTextInput('company', 'Company')
                ->setPrepend('<i class="fas fa-building"></i>');
        $form->createTextInput('phone', 'Phone')
                ->setPrepend('<i class="fas fa-phone"></i>');
        $form->createTextInput('mobile', 'Mobile')
                ->setPrepend('<i class="fas fa-mobile"></i>');
        $form->createSelect('gender', 'Gender')
                ->setOptions(array('none' => 'None', 'male' => 'Male', 'famele' => 'Female'))
                ->setPrepend('<i class="fas fa-venus-mars"></i>');
        $form->createButton("user-data", 'Save')
                ->Class('form-control btn btn-sm btn-success');
        $form->vulues($data->getMe());
        if ($form->isSend()) {
            $this->ProccedUserDataForm($form);
        }
        return $form;
    }

    public function getHeads(\JR\CORE\midleware\users\User $user) {
        $this->data['heads'] = $user->departments->getMyAdmins();
        $this->data['heads']['right'] = $user->rights->getUsersWithRight('users', 'all');
    }

    public function ProccedUserForm(\JR\CORE\helpers\form\FormFactory $form) {
        $values = $form->getValues();
        $user = $this->dep->getUser();
        $user->updateUser($values);
        $this->dep->getSession()->setMessages
                ($this->dep->getTranslations()
                        ->str("Profile has been updated!"), "success");
        $this->dep->getActionLog()->addLog('Users-profile', 'change-profile', json_encode(array("data" => $form->getValues())));
        throw new \JR\CORE\router\RedirectError("user/profile");
    }

    public function ProccedUserDataForm(\JR\CORE\helpers\form\FormFactory $form) {
        $values = $form->getValues();
        $user = $this->dep->getUser();
        $user->personalInfo->updateMe($values);
        $this->dep->getSession()->setMessages
                ($this->dep->getTranslations()
                        ->str("Profile personal data has been updated!"), "success");
        $this->dep->getActionLog()->addLog('Users-profile', 'change-profile-personal', json_encode(array("data" => $form->getValues())));
        throw new \JR\CORE\router\RedirectError("user/profile");
    }

    public function killSession($id, \JR\CORE\midleware\sessions\Session $userSession) {
        if ($userSession->InvalideSession($id)) {
            $this->dep->getSession()
                    ->setMessages($this->dep->
                            getTranslations()
                            ->str('Session has been killed'), 'success');
            $this->dep->getActionLog()->addLog('Users-profile', 'kill-session', json_encode(array("sess_id" => $id)));
        } else {
            $this->dep->getSession()
                    ->setMessages($this->dep->
                            getTranslations()
                            ->str('Something went wrong!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("user/profile");
    }

    public function changePassword(\JR\CORE\midleware\users\User $user) {
        $post = $this->request->getPost();
        if ($post['password'] == $post['password-again']) {
            if (strlen($post['password']) > 4) {
                $user->password->change($post['password']);
                $this->dep->getSession()
                        ->setMessages($this->dep->
                                getTranslations()
                                ->str('Password has been changed'), 'success');
                $this->dep->getActionLog()->addLog('Users-profile', 'change-password');
            } else {
                $this->dep->getSession()
                        ->setMessages($this->dep->
                                getTranslations()
                                ->str('Passwords is too week!'), 'danger');
            }
        } else {
            $this->dep->getSession()
                    ->setMessages($this->dep->
                            getTranslations()
                            ->str('Passwords do not match!'), 'danger');
        }
        throw new \JR\CORE\router\RedirectError("user/profile");
    }

    public function generateInvitationForm($csrf) {
        $form = new \JR\CORE\helpers\form\FormFactory("invitation", $csrf);
        $form->createHidden("inv_id", null);
        $form->createHidden("action", null);
        if ($form->isSend()) {
            bdump($form);
            $this->ProccedInvitationForm($form->getValues());
        }
        return $form;
    }

    public function ProccedInvitationForm($data) {
        $inv = $this->invitations->getOne($data['inv_id']);
        bdump($inv);
        if ($inv['user_id'] != $this->dep->getUser()->getInternalId()) {
            throw new \JR\CORE\router\RedirectToError(403, "This is not your invitation!");
        }
        if ($data['action'] == 'accept') {
            $this->invitations->setStatus($data['inv_id'], "accepted");
            $this->dep->getUser()->departments->addDepartment($inv['department_id'], $inv['role']);
            $this->dep->getUser()->setRoleAuto();
            $this->dep->getNotifications()->createDepInvAccept($inv['admin_id'], $inv['user_id']);
            $this->makeLogAndLeave("Congratulations! You have joined additional department!",
                    "Departments", "accept", ['dep_id' => $inv['department_id'],
                "role" => $inv['role'], "admin_id" => $inv['admin_id'],
                "inv_id" => $inv['id']], "user/profile");
        } else if ($data['action'] == 'deceline') {
            $this->invitations->setStatus($data['inv_id'], "decelined");
            $this->dep->getNotifications()->createDepInvDeceline($inv['admin_id'], $inv['user_id']);
            $this->makeLogAndLeave("The invitation has been declined",
                    "Departments", "deceline", ['dep_id' => $inv['department_id'],
                "role" => $inv['role'], "admin_id" => $inv['admin_id'],
                "inv_id" => $inv['id']], "user/profile");
        }
        throw new \Exception("unsupported operation with invitations");
    }

}
