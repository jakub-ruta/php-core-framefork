<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\user;

/**
 * Description of Register
 *
 * @author jakub
 */
class Register extends \JR\CORE\controler\WebControler {

    public $view = "core.users.register";

//put your code here
    public function execute() {
        $this->data['hide_sidebar'] = true;
        $register_form = $this->createForm();
        if ($register_form->isSend()) {
            $this->proccedRegister($register_form->getValues());
        }
        $this->data['form'] = $register_form;
    }

    protected function createForm() {
        $form = new \JR\CORE\helpers\form\FormFactory("register", $this->dep->getSession()->getCSRFToken());
        $form->createTextInput("username", "Name")
                ->placeholder("Username")
                ->setPrepend('<i class="fas fa-user"></i>')
                ->required();
        $form->createTextInput("mail", "Email")
                ->placeholder("Email")
                ->setType('email')
                ->setPrepend('<i class="fas fa-at"></i>')
                ->required();
        $form->createTextInput("password", "Password")
                ->placeholder("Password")
                ->setType("password")
                ->setPrepend('<i class="fas fa-lock"></i>')
                ->required();
        $form->createButton("register", "Register");
        return $form;
    }

    protected function proccedRegister($values) {
        $count = $this->dep->getActionLog()->getCountOf('User-register', 'register', 60 * 60 * 24, $this->request->getUserIP());
        if ($count > $_ENV['max_reg_per_ip']) {
            throw new \JR\CORE\router\RedirectToError(401, "Count of registration from this ip exceed limit");
        }
        $this->dep->getActionLog()->addLog('User-register', 'register',
                json_encode(array("user_name" => $values['username'],
            "user_email" => $values['mail'])), 6);
        try {
            $id = $this->dep->getUserFactory()->registerUser(strtolower($values['username']),
                    $values['mail'], $values['password']);
        } catch (\JR\CORE\midleware\DuplicateRowException $exc) {
            $this->dep->getSession()->setMessages("This name is already taken.", "danger");
        }
        if (is_numeric($id)) {
            $this->dep->getSession()->setMessages("Account has been registered", "success");
            $this->dep->getSession()->setMessages("You can now log in", "primary");
            throw new \JR\CORE\router\RedirectError("login");
        } else {
            $this->dep->getSession()->setMessages("Registration has not been successfull", "danger");
        }
    }

}
