<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\tech_admin;

/**
 * Description of MailTool
 *
 * @author jakub
 */
class MailTool extends \JR\CORE\controler\WebControler {

    public $view = 'core.tech_admin.mail_tool';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'mail_tool';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 1;

    public function execute() {
        $mailUtils = $this->dep->getEmailTool();
        $users = $this->getUsers($mailUtils);
        $searchForm = $this->makeSearchForm($mailUtils, $users);
        $this->data['search_form'] = $searchForm;
        if ($searchForm->isSend()) {
            $form_values = $this->filter_search($searchForm->getValues(), $users);
            $this->dep->getActionLog()->addLog('Mail-tool', 'search', $this->dep->getRequest()->getJSONQuery());
            $this->data['results'] = $mailUtils->getData($this->request->getLimit(),
                    $this->request->getPage(), $form_values);
        }
    }

    public function makeSearchForm(\JR\CORE\helpers\mail\EmailTool $mailUtils, $users) {
        $user = array_merge([['internal_id' => '-1', 'login_name' => "--All--"]],
                $users);
        $form = new \JR\CORE\helpers\form\FormFactory('search', $this->dep->getSession()->getCSRFToken());
        $form->createSelect('user_id', 'User')
                ->value($this->dep->getUser()->getInternalId())
                ->setOptions($user, 'internal_id', 'login_name')
                ->required();
        $form->setForm_class('form-inline-md');
        $form->createTextInput("from", "From")
                ->setType("datetime-local")
                ->required()
                ->value(date("Y-m-d\TH:i", time() - 60 * 60 * 24 * 10));
        $form->createTextInput("to", "To")
                ->setType("datetime-local")
                ->required()
                ->value(date("Y-m-d\TH:i"));
        $form->createButton('mail_tool', 'Filter');
        return $form;
    }

    public function filterUserToMyDepartment($users) {
        $ids = $this->dep->getUser()->departments->getUsersIdsInMyDepartments(4);
        foreach ($users as $key => $value) {
            if (!in_array($value['internal_id'], $ids)) {
                unset($users[$key]);
            }
        }
        return $users;
    }

    public function filter_search($values) {
        $rights = $this->dep->getUser()->rights;
        if ($rights->can("mail_tool", 'all', false) && $values['user_id'] == -1) {
            $values['user_ids'] = -1;
        } else if ($rights->can("mail_tool", 'my', false) && $values['user_id'] == -1) {
            $values['user_ids'] = $this->dep->getUser()->departments->getUsersIdsInMyDepartments(4);
        } else {
            $values['user_ids'] = [0 => $values['user_id']];
        }
        return $values;
    }

    public function getUsers(\JR\CORE\helpers\mail\EmailTool $mailUtils) {
        if ($this->dep->getUser()->rights->can("mail_tool", 'all', false)) {
            $user = $mailUtils->getDistinctUsers();
        } elseif ($this->dep->getUser()->rights->can("mail_tool", 'my', false)) {
            $user = $this->filterUserToMyDepartment($mailUtils->getDistinctUsers());
        } else {
            $user = [['internal_id' => $this->dep->getUser()->getInternalId(),
            'login_name' => $this->dep->getUser()->getLoginName()]];
        }
        return $user;
    }

}
