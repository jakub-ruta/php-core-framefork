<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\tech_admin;

/**
 * Description of DeamonControler
 *
 * @author jakub
 */
class DeamonControler extends \JR\CORE\controler\WebControler {

    public $view = 'core.tech_admin.deamon.menu';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath()[2]) {
            case 'task':
                $new = new deamon\Task($this->dep);
                break;
            case 'queue':
                $new = new deamon\Queue($this->dep);
                break;
            default:
                $new = new deamon\Deamon($this->dep);
                break;
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
