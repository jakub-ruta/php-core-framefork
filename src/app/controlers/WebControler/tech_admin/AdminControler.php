<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\tech_admin;

/**
 * Description of AdminControler
 *
 * @author jakub
 */
class AdminControler extends \JR\CORE\controler\WebControler {

    public $view = 'core.tech_admin.menu';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    //put your code here
    public function execute() {
        switch ($this->request->getParsedPath()[1]) {
            case 'action_log':
                $new = new ActionLog($this->dep);
                break;
            case 'deamon':
                $new = new DeamonControler($this->dep);
                break;
            case 'translations':
                $new = new Translations($this->dep);
                break;
            case 'mail-tool':
                $new = new MailTool($this->dep);
                break;
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
