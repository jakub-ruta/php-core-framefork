<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\users;

/**
 * Description of AllUsers
 *
 * @author jakub
 */
class AllUsers extends \JR\CORE\controler\WebControler {

    public $view = 'core.admin.users.users';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'users';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    public function execute() {
        $this->data['users'] = $this->getVisibleUsers();
    }

    public function getVisibleUsers() {
        $user = $this->dep->getUser();
        if ($user->rights->can("users", 'all', false) && $user->getRole() >= 6) {
            return $user->getAll($this->request->getLimit(),
                            $this->request->getPage());
        } else if ($user->getRole() > 9 && $user->rights->can("rights", 'overRide', false) == 1) {
            $this->dep->getSession()->setMessages("right: \"users - all\"", "primary", "Master developer rights overide");
            return $user->getAll($this->request->getLimit(),
                            $this->request->getPage());
        } else if ($user->getRole() >= 4) {
            return [$user->getUserInDepartements($user->departments->getMyDepartementsIds(4))];
        } else {
            return [[$user->getOne($user->getInternalId())]];
        }
    }

}
