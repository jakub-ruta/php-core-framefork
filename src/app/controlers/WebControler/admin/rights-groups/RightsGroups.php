<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\admin\rights;

/**
 * Description of RightsGroups
 *
 * @author jakub
 */
class RightsGroups extends \JR\CORE\controler\WebControler {

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'rights';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 5;

    public function execute() {
        if (is_numeric($this->request->getParsedPath()[2])) {
            $new = new OneGroup($this->dep, $this->request->getParsedPath()[2]);
        } else {
            switch ($this->request->getParsedPath()[2]) {
                case 'new':
                    $new = new NewGroup($this->dep);
                    break;
                default:
                    $new = new AllGroups($this->dep);
                    break;
            }
        }
        if (isset($new)) {
            $new->execute();
            $this->data = $new->data;
            $this->view = $new->view;
        }
    }

}
