<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\debug;

/**
 * Description of PHP-info
 *
 * @author jakub
 */
class PhpInfo extends \JR\CORE\controler\WebControler {

    public $view = 'core.debug.phpInfo';

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = 'tech_admin';

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = true;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = 9;

    public function execute() {

    }

}
