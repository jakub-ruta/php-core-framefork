<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\cron\tasks\mail;

/**
 * Description of TaskDispatch
 *
 * @author jakub
 */
class MailTask extends \JR\CORE\controlers\cron\tasks\CronTask {

    /**
     *
     * @var \JR\CORE\helpers\mail\EmailTool
     */
    protected $utils;

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        parent::__construct($dep);
        $this->utils = new \JR\CORE\helpers\mail\EmailTool($this->dep->getDB());
    }

    function dispatch() {
        $this->message = $this->utils->processQueue();
    }

}
