<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\cron\tasks;

/**
 * Description of CronTask
 *
 * @author jakub
 */
abstract class CronTask {

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;

    /**
     *
     * @var \JR\CORE\config\Config
     */
    protected $config;

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;
    protected $message;

    function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        $this->request = $dep->getRequest();
        $this->db = $dep->getDB();
        $this->dep = $dep;
        $this->config = $dep->getConfig();
    }

    function getMessage() {
        return $this->message;
    }

}
