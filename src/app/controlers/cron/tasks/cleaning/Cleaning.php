<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controlers\cron\tasks\cleaning;

/**
 * Description of TaskDispatch
 *
 * @author jakub
 */
class Cleaning extends \JR\CORE\controlers\cron\tasks\CronTask {

    public function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        parent::__construct($dep);
    }

    function actionLog() {
        $action = $this->dep->getActionLog();
        $this->message = $action->cleanOld();
    }

}
