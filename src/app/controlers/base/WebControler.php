<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controler;

/**
 * Description of WebControler
 *
 * @author jakub
 */
abstract class WebControler extends Controler {

    /**
     *
     * @var string view that will be displaied to user
     */
    public $view = null;

    public function prepareSharedData() {
        $this->data['base']['langs'] = $this->dep->getTranslations()->getLangs();
        $this->data['base']['lang'] = $this->dep->getTranslations()->getSelectedLang();
        $this->data['base']['notifications'] = $this->dep->getNotifications()->getForUser($this->dep->getUser()->getInternalId());
        $this->data['base']['messages'] = $this->dep->getSession()->getMessages();
        $this->data['base']['user'] = $this->dep->getUser();
        $this->data['admin_levels'] = $this->data['base']['user']->admin_levels;
        $this->data['base']['admin'] = $this->dep->getAdmin();
        $this->data['base']['request'] = $this->request;
        $this->data['csrf'] = $this->dep->getSession()->getCSRFToken();
        $this->data['config'] = $this->config;
        $this->data['menu'] = new \JR\CORE\helpers\menu\Menu($this->request, $this->dep->getUser());
        $this->data['assets'] = new \JR\CORE\helpers\Assets($this->request->getBasePath(), $this->config->getVersion());
    }

    protected function makeLogAndLeave($vissibleText, $logTool, $logAction, $logData, $redirectPath, $log_view = 1, $color = "success") {
        $this->dep->getSession()->setMessages
                ($this->dep->getTranslations()
                        ->str($vissibleText), $color);
        $this->dep->getActionLog()->addLog($logTool, $logAction,
                json_encode($logData));
        throw new \JR\CORE\router\RedirectError($redirectPath);
    }

    protected function getCSRF() {
        return $this->dep->getSession()->getCSRFToken();
    }

    protected function setMessage($message, $color = "success", $head = null, $url = null) {
        $this->dep->getSession()->setMessages
                ($this->dep->getTranslations()
                        ->str($message), $color, $head, $url);
    }

}
