<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controler;

/**
 * Description of APIControler
 *
 * @author jakub
 */
abstract class APIControler extends Controler {

    /**
     *
     * @var array array of error messages that should be passed to view
     */
    public $msg_error = null;

    /**
     *
     * @var array array of warning messages that should be passed to view
     */
    public $msg_warning = null;

    /**
     *
     * @var array array of success messages that should be passed to view
     */
    public $msg_success = null;

}
