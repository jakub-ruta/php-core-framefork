<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\controler;

/**
 * Description of Controler
 *
 * @author jakub
 */
abstract class Controler {

    /**
     *
     * @var \MysqliDb
     */
    protected $db;

    /**
     *
     * @var \JR\CORE\request\request
     */
    protected $request;

    /**
     *
     * @var \JR\CORE\config\Config
     */
    protected $config;

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    /**
     *
     * @var int from -1 to 10 - represent admin main role
     */
    protected $requiredAdminLevel = -1;

    /**
     *
     * @var string|null right internal name to view this
     */
    protected $rightToView = null;

    /**
     *
     * @var string action for right | default view
     */
    protected $subRightToView = 'view';

    /**
     *
     * @var bool represent if user have to be logged in to use it
     */
    protected $haveToBeUserLoggedIn = false;

    /**
     *
     * @var array array of data that should be passed to view
     */
    public $data = null;

    function __construct(\JR\CORE\helpers\dependencies\DependenciContainer $dep) {
        $this->request = $dep->getRequest();
        $this->db = $dep->getDB();
        $this->dep = $dep;
        $this->config = $dep->getConfig();
        $this->checkRights();
    }

    abstract function execute();

    /**
     * Check if user have to be logged in
     * Check if user has required admin level
     * Check if user has required right
     * @throws LoggedInPolicy
     * @throws AdminLevelPolicy
     * @throws RightViolationException
     */
    protected function checkRights() {
        $user = $this->dep->getUser();
        if ($this->haveToBeUserLoggedIn && !$user->isLoggedIn()) {
            throw new \JR\CORE\router\LoggedInPolicy();
        }
        if ($this->requiredAdminLevel > $user->getRole()) {
            throw new \JR\CORE\router\AdminLevelPolicy($this->requiredAdminLevel);
        }
        if ($this->rightToView != null) {
            try {
                $user->rights->can($this->rightToView, $this->subRightToView);
            } catch (\JR\CORE\midleware\rights\RightViolationException $ex) {
                if ($user->getRole() > 9 && $user->rights->can("rights", 'overRide', false) == 1) {
                    $this->dep->getSession()->setMessages("right: \"" . $ex->getMessage() . "\"", "primary", "Master developer rights overide");
                } else {
                    throw new \JR\CORE\midleware\rights\RightViolationException($ex->getMessage());
                }
            }
        }
    }

}
