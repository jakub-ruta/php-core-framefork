<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\cli\main;

/**
 * Description of CLI_interface
 *
 * @author jakub
 */
class CLI_interface {

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    public function __construct($path) {
        $this->dotenv = \Dotenv\Dotenv::createImmutable($path, 'config.env');
        $this->dotenv->load();
        $this->dep = new \JR\CORE\helpers\dependencies\DependenciContainer();
        $this->dep->makeMeStatic();
    }

    public function execute() {
        $test = new \Ahc\Cli\Application("PHP CORE framework",
                $this->dep->getConfig()->getCoreVersion());

        $test->command("migrate", "Start databse migration")
                ->option("-fresh --fresh", "Drop tables before migration")
                ->action([new \JR\CORE\cli\main\CLI_actions, 'databaseMigration']);
        $test->command("cache", "Flush cache / Delete logs")
                ->option("-w --views", "Delete views cache")
                ->option("-t --tracy", "Delete tracy log")
                ->action([new \JR\CORE\cli\main\CLI_actions, 'cacheFlush']);
        $test->command("user::create", "Create user")
                ->option("-d --dev", "Mark as master developer")
                ->action([new \JR\CORE\cli\main\CLI_actions, 'createUser']);
        $test->handle($_SERVER['argv']);
    }

}
