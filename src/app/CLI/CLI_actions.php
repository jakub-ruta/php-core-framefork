<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\cli\main;

/**
 * Description of CLI_actions
 *
 * @author jakub
 */
class CLI_actions {

    /**
     *
     * @var \Ahc\Cli\Output\Color
     */
    public $color;

    /**
     *
     * @var \JR\CORE\helpers\dependencies\DependenciContainer
     */
    protected $dep;

    public function __construct() {
        $this->color = new \Ahc\Cli\Output\Color;
        $this->dep = \JR\CORE\helpers\dependencies\DependenciContainer::gI();
    }

    function databaseMigration($fresh) {
        echo($this->color->info("Starting migrations \n"));
        if ($fresh) {
            echo($this->color->warn("Deleting all tables! \n"));
        }
        $Schema = new \JR\CORE\database\migrations\Schema($this->dep->getDB(), $fresh);
        $Schema->loadMigrations();
        $Schema->execute();
        echo($this->color->ok("Migrations has been done \n"));
    }

    function cacheFlush($views, $tracy) {
        $prefix = "";
        if ($_SERVER["SCRIPT_FILENAME"] != "cli.php") {
            $prefix = str_replace("cli.php", "", $_SERVER['SCRIPT_FILENAME']);
        }
        if ($views) {
            array_map('unlink', array_filter((array) glob($prefix . "app/temp/views/*")));
        }
        if ($tracy) {
            array_map('unlink', array_filter((array) glob($prefix . "app/temp/tracy/*")));
        }
    }

    function createUser($dev) {
        $prefix = "";
        if ($_SERVER["SCRIPT_FILENAME"] != "cli.php") {
            $prefix = str_replace("cli.php", "", $_SERVER['SCRIPT_FILENAME']);
        }
        $interactor = new \Ahc\Cli\IO\Interactor();
        $nameValidator = function ($value) {
            if (\strlen($value) < 5) {
                throw new \InvalidArgumentException('Name should be atleast 5 chars');
            }
            return $value;
        };
        $name = $interactor->prompt('Name', 'admin', $nameValidator, 5);
        $passValidator = function ($pass) {
            if (\strlen($pass) < 5) {
                throw new \InvalidArgumentException('Password too short');
            }
            return $pass;
        };
        $pass = $interactor->promptHidden('Password', $passValidator, 2);
        \JR\CORE\midleware\users\User::registerUserViaCLI($name, $name . "@local", $pass, $dev, $this->dep->getDB());
    }

}
