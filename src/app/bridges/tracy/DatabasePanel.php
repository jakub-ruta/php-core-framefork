<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use JR\CORE\views;

/**
 * Description of DatabasePanel
 *
 * @author jakub
 */
class DatabasePanel implements \Tracy\IBarPanel {

    /**
     *
     * @var \MysqliDb
     */
    protected $db;
    protected $count = 0;
    protected $total_time = 0;

    public function __construct(\MysqliDb $db) {
        $this->db = $db;
    }

    public function getPanel(): ?string {
        $view = new views\RawView($this->getPanelData(), "bridge.tracyBar.Database.panel");
        return $view->renderToString();
    }

    public function getTab(): ?string {
        $view = new views\RawView($this->getTabData(), "bridge.tracyBar.Database.tab");
        return $view->renderToString();
    }

    public function getTabData() {
        $this->prepareData();
        return array("count" => $this->count, "time" => $this->total_time);
    }

    public function prepareData() {
        foreach ($this->db->trace as $u) {
            $this->count++;
            $this->total_time += $u[1];
        }
    }

    public function getPanelData() {
        return array("count" => $this->count,
            "time" => $this->total_time,
            "queries" => $this->db->trace);
    }

}
