<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JR\CORE\bridges\tracy;

use JR\CORE\views;

/**
 * Description of DatabasePanel
 *
 * @author jakub
 */
class TranslationsPanel implements \Tracy\IBarPanel {

    /**
     *
     * @var \JR\CORE\helpers\langs\Lang
     */
    protected $lang;
    protected $count = 0;
    protected $total_time = 0;

    public function __construct(\JR\CORE\helpers\langs\Lang $lang) {
        $this->lang = $lang;
    }

    public function getPanel(): ?string {
        $view = new views\RawView($this->getPanelData(), "bridge.tracyBar.Translations.panel");
        return $view->renderToString();
    }

    public function getTab(): ?string {
        $view = new views\RawView($this->getTabData(), "bridge.tracyBar.Translations.tab");
        return $view->renderToString();
    }

    public function getTabData() {
        return array("lang" => $this->lang->getSelectedLang());
    }

    public function getPanelData() {
        return array("Missing" => $this->lang->getMissing_strings(),
            "Prefered" => $this->lang->getSelectedLang(),
            "Available" => $this->lang->getAvailableLangs(),
            "Strings" => $this->lang->getStrings());
    }

}
